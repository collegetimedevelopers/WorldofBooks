package com.collegetime.theworldofbooks.Executors;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;

import com.collegetime.theworldofbooks.Adapters.SearchResultAdapter;
import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.EventBus.HideFabEvent;
import com.collegetime.theworldofbooks.LoadBookDetail;
import com.collegetime.theworldofbooks.Models.LinksModel;
import com.collegetime.theworldofbooks.EventBus.showTitleEvent;
import com.collegetime.theworldofbooks.R;
import com.collegetime.theworldofbooks.Services.DownloadService;
import com.collegetime.theworldofbooks.ui.home.HomeFragment;
import com.collegetime.theworldofbooks.ui.home.HomeViewModel;

import org.greenrobot.eventbus.EventBus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;


public class Search extends AsyncTask<Void,Void,Document> {
    String key;
    String url;
    Document doc;
    Context context;
    String column;
    AlertDialog dialog;
    AlertDialog.Builder builder;
    ArrayList<LinksModel>link;
    boolean connected = false;

    public Search(Context context, String key, String column)
    {
        this.context = context;
        this.key = key;
        this.column = column;
        builder = new AlertDialog.Builder(context,R.style.SearchingDialogStyle);
        builder.setCancelable(false);
//        @SuppressLint("InflateParams") View view = LayoutInflater.from(context).inflate(R.layout.layout_searching,null);

        builder.setView(R.layout.layout_searching);

        dialog = builder.create();

        link = new ArrayList<>();

    }

    private void prepareUrl()
    {
        switch (column)
        {
            case "Default":
                column = "def";
                break;
            case "Author":
                column = "author";
                break;
            case  "ISBN":
                column = "identifier";
                break;
            case "Publisher":
                column = "publisher";
                break;
            case "Series":
                column = "series";
                break;
            case "Title":
                column = "title";
                break;
            default: column = "def";
        }
        key = key.replace(' ','+');
        url = Common.SEARCH_URL+key+Common.SEARCH_URL_ATTRIBUTES+column;


    }


    @Override
    protected Document doInBackground(Void... voids) {
        if(connected) {
            prepareUrl();
            int tries = 0;
            Log.v("URL", url);
            while (tries < 3) {
                try {
                    doc = Jsoup.connect(url).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .timeout(10000).get();

                } catch (IOException e) {
                    e.printStackTrace();

                }
                tries++;


            }
            return doc;
        }
        else
        {
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


        try {
            dialog.show();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        InetAddress ip = null;

        try {
            ip = InetAddress.getByName(Common.SEARCH_DOMAIN);
            if(ip!=null)
            {
                connected = true;
                Log.v("CONN", "Connected to " + ip);
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.v("CONN", "HOST_RES_ERR : " + e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(Document document) {
        super.onPostExecute(document);

        try {
            if (dialog.isShowing()&& dialog!=null)
                  dialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        dialog.dismiss();
        if(document!=null)
        {
            Elements tables = document.select("table");
            String no_results = tables.get(1).select("tbody").select("tr").select("td").first().text().trim();
            String[] result = no_results.split(" ");
            if(result[0].equals("0"))
            {
                final AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.SearchingDialogStyle);
                builder.setView(R.layout.layout_no_result);
                builder.setCancelable(false).setPositiveButton("TRY AGAIN", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.dismiss();
                builder.show();
                HomeFragment.SHOW_TITLE_ON_RESUME=false;

                HomeFragment.parent_title_of_title.setVisibility(View.GONE);
                EventBus.getDefault().postSticky(new HideFabEvent(false));
                HomeFragment.getPromoBook();
            }
            else {

                PrepareSearchResult prepareSearchResult = new PrepareSearchResult(tables);
                prepareSearchResult.loadBooks();

                SearchResultAdapter adapter = new SearchResultAdapter(context,prepareSearchResult.list);
                HomeViewModel.resultsList.setValue(prepareSearchResult.list);
                HomeViewModel.links.setValue(prepareSearchResult.links);
                HomeFragment.searchList.setAdapter(adapter);
                HomeFragment.links = prepareSearchResult.links;
                EventBus.getDefault().postSticky(new showTitleEvent(true));
                EventBus.getDefault().postSticky(new HideFabEvent(true));
                dialog.dismiss();
                try {
                    prepareSearchResult.finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }

        }
        else
        {
        EventBus.getDefault().postSticky(new showTitleEvent(false));
            dialog.dismiss();
            final AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.SearchingDialogStyle);
            builder.setView(R.layout.layout_error);
            builder.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            final AlertDialog noDataDialog = builder.create();
           noDataDialog.show();
            HomeFragment.parent_title_of_title.setVisibility(View.GONE);
           HomeFragment.getPromoBook();


        }

    }


}
