package com.collegetime.theworldofbooks.ui.home;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.collegetime.theworldofbooks.Adapters.PromoBookAdapter;
import com.collegetime.theworldofbooks.Adapters.SearchResultAdapter;
import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.EventBus.HideFabEvent;
import com.collegetime.theworldofbooks.EventBus.showTitleEvent;
import com.collegetime.theworldofbooks.Executors.Search;
import com.collegetime.theworldofbooks.Models.LinksModel;
import com.collegetime.theworldofbooks.Models.PromoBookModel;
import com.collegetime.theworldofbooks.Models.SearchResult;
import com.collegetime.theworldofbooks.R;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class HomeFragment extends Fragment implements AdapterView.OnItemSelectedListener{

    private static Context context ;
    private HomeViewModel homeViewModel;
    EditText keyword;
    Button btn_search;
    String column;
    Spinner search_by_spinner;
    AppBarLayout searchLayout;
    public static RecyclerView searchList;
    public static ArrayList<LinksModel> links;
    public static ArrayList<SearchResult>resultArrayList;
    TextView publisher;
    TextView title;
    public static LinearLayout parent_title_of_title;
    Search search;
    AppBarLayout.Behavior behavior;
    ArrayList<PromoBookModel>promoBookModels = new ArrayList<>();
    PromoBookModel promoBookModel;
    public static boolean SHOW_TITLE_ON_RESUME;


    SharedPreferences preferences;
    SharedPreferences.Editor editor;


    public static void promoBookSearch(String key)
    {

        Search s = new Search(context,key,"def");
        s.execute();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        context = getContext();
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
         View root = inflater.inflate(R.layout.fragment_home, container, false);

         final View view = root;

        preferences = getActivity().getSharedPreferences("AppData", Context.MODE_PRIVATE);
        editor = preferences.edit();

        searchLayout= root.findViewById(R.id.app_bar_layout);

        ((CoordinatorLayout.LayoutParams) searchLayout.getLayoutParams()).setBehavior(new AppBarLayout.Behavior() {});




       CoordinatorLayout.LayoutParams scroll = (CoordinatorLayout.LayoutParams) searchLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) scroll.getBehavior();
        if(behavior !=null)
        {
            System.out.println("------------------- NOT NULL--------------------");
            behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return false;
                }
            });
        }
        else
        {
            System.out.println("-------------------NULL--------------------");
        }


        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<ArrayList<SearchResult>>() {

            @Override
            public void onChanged(ArrayList<SearchResult> searchResults) {
                resultArrayList =  searchResults;
                SearchResultAdapter adapter = new SearchResultAdapter(getContext(),searchResults);
                searchList.setAdapter(adapter);
                SHOW_TITLE_ON_RESUME= true;


            }
        });
        homeViewModel.getLinks().observe(getViewLifecycleOwner(), new Observer<ArrayList<LinksModel>>() {
            @Override
            public void onChanged(ArrayList<LinksModel> linksModels) {
                links = linksModels;
            }
        });
        final Context context = getContext();


        promoBookModels = new ArrayList<>();
        promoBookModel  = new PromoBookModel();

        keyword = root.findViewById(R.id.key);
        btn_search= root.findViewById(R.id.fetch);
        search_by_spinner= root.findViewById(R.id.search_by);
        searchList = root.findViewById(R.id.recycler_view_list);

        publisher= root.findViewById(R.id.publisher);
        title=root.findViewById(R.id.title);
        parent_title_of_title=root.findViewById(R.id.parent_layout_of_title);
        searchList.setLayoutManager(new LinearLayoutManager(getContext()));
        links = new ArrayList<>();

        final ArrayAdapter<CharSequence> search_list =  ArrayAdapter.createFromResource(context,R.array.search_by,R.layout.spinner_item);

        search_list.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        search_by_spinner.setAdapter(search_list);
        search_by_spinner.setOnItemSelectedListener(HomeFragment.this);

        appbarLayoutListener();

        // CODE FOR ADDING SEARCH ON KEYBOARD AND LISTENING TO KEYBOARD SEARCH BUTTON
        keyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    actionSearch(view);
                }
                return false;
            }
        });
        setPromoBooks();

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(keyword.getText().toString().length()<2)
                {

                    keyword.setError("Minimum 2 characters required.");
                    keyword.requestFocus();
                    return;
                }

                else
                {
                    if(column.equals("ISBN"))
                    {
                        if(keyword.getText().toString().length()<10)
                        {
                            keyword.setError("Short ISBN");
                            keyword.requestFocus();
                            return;
                        }
                    }

                    searchList.setAdapter(null);
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                    assert imm != null;
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    Common.SEARCH_COUNT++;

                    search = new Search(HomeFragment.this.getActivity(), keyword.getText().toString().trim(), column);
                    search.execute();
//                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }


            }
        });
        searchList.setClickable(true);
        return root;
    }

    private void actionSearch(View v) {
        final Context context = getContext();
        if(keyword.getText().toString().length()<2)
        {

            keyword.setError("Minimum 2 characters required.");
            keyword.requestFocus();
            return;
        }

        else
        {
            if(column.equals("ISBN"))
            {
                if(keyword.getText().toString().length()<10)
                {
                    keyword.setError("Short ISBN");
                    keyword.requestFocus();
                    return;
                }
            }

            searchList.setAdapter(null);
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            Common.SEARCH_COUNT++;
            search = new Search(HomeFragment.this.getActivity(), keyword.getText().toString().trim(), column);
            search.execute();
//                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void setPromoBooks() {
        // SET LIST OF PROMO BOOKS FROM SERVERS
        if (Common.ShowPromo) {
            if (Common.promoBookAdapter != null) {
                Common.promoBookAdapter.shuffleBooks();

                searchList.setAdapter(Common.promoBookAdapter);
                SHOW_TITLE_ON_RESUME = false;
            }
        }
    }


    public static void getPromoBook() {

        if (Common.ShowPromo) {
            Common.COUNT_BACK_PRESS=true;// means user is pressing back button to go back no we need to consider it

            if (Common.promoBookAdapter != null) {
                searchList.setAdapter(Common.promoBookAdapter);
            }
        }
    }


    private void appbarLayoutListener() {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        column = parent.getItemAtPosition(position).toString();
        if(column.equals("ISBN"))
        {
            keyword.setText("");
            keyword.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
        else
        {
            keyword.setText("");
            keyword.setInputType(InputType.TYPE_CLASS_TEXT);
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        column="Default";
    }


    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    public void showTitle(showTitleEvent event)
    {
        if (event.isSuccess())
        {
            parent_title_of_title.setVisibility(View.VISIBLE);
            title.setVisibility(View.VISIBLE);
            publisher.setVisibility(View.VISIBLE);


        }
        else if (!event.isSuccess()){
            parent_title_of_title.setVisibility(View.GONE);
            title.setVisibility(View.GONE);
            publisher.setVisibility(View.GONE);
        }
//
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);

            EventBus.getDefault().postSticky(new HideFabEvent(false));
            EventBus.getDefault().postSticky(new showTitleEvent(false));

        }

        if (Common.isConnected(getContext())) {
            updateLastOnline();

        }

        Common.CURRENT_FRAGMENT=R.id.nav_home;
    }

    @Override
    public void onPause() {

              EventBus.getDefault().removeAllStickyEvents();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SHOW_TITLE_ON_RESUME){
            EventBus.getDefault().postSticky(new showTitleEvent(true));
            EventBus.getDefault().postSticky(new HideFabEvent(true));
        }
    }

    private void updateLastOnline()  {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        editor.putString("lastOnline",formattedDate);
        editor.apply();
        System.out.println("-----------------------------DATE :"+formattedDate+"---------------------------");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE,7);

        Date validTill = cal.getTime();
        String validDate = df.format(validTill);
        editor.putString("validTill",validDate);
        editor.apply();
        System.out.println("---------------------------"+"VALID : "+validDate+"-----------------------");

    }

}

