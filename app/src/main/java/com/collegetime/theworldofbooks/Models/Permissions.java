package com.collegetime.theworldofbooks.Models;

public class Permissions {
  private boolean ShowPromo,ShowCover,AllowDownload;

    public Permissions() {
    }

    public Permissions(boolean ShowPromo, boolean ShowCover, boolean AllowDownload) {
      this.ShowPromo = ShowPromo;
       this.ShowCover = ShowCover;
        this.AllowDownload = AllowDownload;
    }

    public boolean isShowPromo() {
        return ShowPromo;
    }

    public void setShowPromo(boolean showPromo) {
        ShowPromo = showPromo;
    }

    public boolean isShowCover() {
        return ShowCover;
    }

    public void setShowCover(boolean showCover) {
        ShowCover = showCover;
    }

    public boolean getAllowDownload() {
        return AllowDownload;
    }

    public void setAllowDownload(boolean allowDownload) {
        AllowDownload = allowDownload;
    }
}
