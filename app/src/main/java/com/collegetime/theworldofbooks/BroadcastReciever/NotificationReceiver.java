package com.collegetime.theworldofbooks.BroadcastReciever;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.HomeActivity;
import com.collegetime.theworldofbooks.LoadBookDetail;
import com.collegetime.theworldofbooks.R;
import com.collegetime.theworldofbooks.Services.DownloadService;

import static com.thin.downloadmanager.DownloadManager.STATUS_PENDING;

public class NotificationReceiver extends BroadcastReceiver {
    String CHANNEL_ID = "Download cancled";
    NotificationChannel channel;
    NotificationCompat.Builder builder;
    NotificationManagerCompat notificationManager;

    @Override
    public void onReceive(Context context, Intent intent) {

        int id = intent.getIntExtra("id", 101);
        Toast.makeText(context,"In Notfication Receiver",Toast.LENGTH_SHORT).show();

        boolean cancelDownload = intent.getBooleanExtra(Common.CANCEL_DOWNLOAD, false);
        boolean pauseDownload = intent.getBooleanExtra(Common.PAUSE_DOWNLOAD, false);
        boolean resumeDownload = intent.getBooleanExtra(Common.RESUME_DOWNLOAD,false);

        System.out.println("Cancel value = "+cancelDownload+" pause value = "+pauseDownload);
        if (cancelDownload) {
            int Status= Common.thinDownloadManager.cancel(id);
            Toast.makeText(context,"In Cancel Download ",Toast.LENGTH_SHORT).show();

            System.out.println("============================================================ In Cancel Download");
            if (Status==1)
            {
                Toast.makeText(context,"Download Cancelled",Toast.LENGTH_SHORT).show();
                System.out.println("============================================================  Download Cancelled");
            }
            else {
                Toast.makeText(context,"Failed to Cancel Download ",Toast.LENGTH_SHORT).show();

                System.out.println("============================================================Failed to cancel Download");
            }

            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(id);
        }


        // NOT IN USE  BECAUSE LIBRARY DOEN'T SUPPORTS PAUSE AND RESUME FEATURE PROPERLY

        if (pauseDownload)
        {
            System.out.println("============================================================In Pause Download");
            Toast.makeText(context,"In Pause Download ",Toast.LENGTH_SHORT).show();
            Common.thinDownloadManager.pause(id);

               System.out.println("============================Download Paused================================");

               Toast.makeText(context,"Download Paused",Toast.LENGTH_SHORT).show();



         // creating notification with resume button after pause



            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                channel = new NotificationChannel(CHANNEL_ID, "Download", NotificationManager.IMPORTANCE_LOW);
                channel.setDescription("Download");
                NotificationManager manager = context.getSystemService(NotificationManager.class);
                assert manager != null;
                manager.createNotificationChannel(channel);
            }

            notificationManager = NotificationManagerCompat.from(context);


            Intent openApp= new Intent(context, HomeActivity.class);
            openApp.putExtra("isInternetAvailable","No");
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addNextIntentWithParentStack(openApp);
            PendingIntent resultIntent  = stackBuilder.getPendingIntent(id,PendingIntent.FLAG_UPDATE_CURRENT);

            Intent i = new Intent(context, NotificationReceiver.class);
            i.putExtra("id", id);
            i.putExtra(Common.CANCEL_DOWNLOAD,true);
            PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(context, id, i, PendingIntent.FLAG_UPDATE_CURRENT);

            Intent pause = new Intent(context, NotificationReceiver.class);
            pause.putExtra("id", id);
            pause.putExtra(Common.PAUSE_DOWNLOAD, false);
            pause.putExtra(Common.RESUME_DOWNLOAD,true);
            PendingIntent pausePendingIntent = PendingIntent.getBroadcast(context, id, pause, PendingIntent.FLAG_UPDATE_CURRENT);

            builder = new NotificationCompat.Builder(context, CHANNEL_ID).setContentTitle("Download Paused").setSmallIcon(R.drawable.ic_file_download_black_24dp);
            builder.setPriority(Notification.PRIORITY_LOW);

            // this intent is to open app on notification click because the book is not shown yet in downloads so
            // we will no open downloads but open app
            builder.setContentIntent(resultIntent);
            builder.addAction(R.drawable.ic_cancel_black_24dp,"Cancel", cancelPendingIntent);
            builder.addAction(R.drawable.ic_baseline_pause_24,"Resume",pausePendingIntent);

//            Notification notification = builder.build(); // this code is to make the notification sticky ( means will not remove from tray on swiped or on clearing notficaiton)
//            notification.flags |= Notification.FLAG_NO_CLEAR;
            notificationManager.notify(id, builder.build());


        }



//        if (resumeDownload)
//        {
//            Common.thinDownloadManager.add()
//        }

//            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//            manager.set(id);

//        createNotificationChannel(context);
//        NotificationManagerCompat mNotification = NotificationManagerCompat.from(context);
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID).setContentText("Download Cancled").setContentTitle("File Download").setSmallIcon(R.drawable.ic_file_download_black_24dp);
//        builder.setPriority(Notification.PRIORITY_LOW);
//        mNotification.notify(0, builder.build());
    }

    private void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel = new NotificationChannel(CHANNEL_ID, "Download", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription("Download");
            NotificationManager manager = context.getSystemService(NotificationManager.class);
            assert manager != null;
            manager.createNotificationChannel(channel);
        }
    }
}
