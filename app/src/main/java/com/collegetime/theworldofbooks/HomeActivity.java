package com.collegetime.theworldofbooks;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.Database.BookDataSource;
import com.collegetime.theworldofbooks.Database.BookDatabase;
import com.collegetime.theworldofbooks.Database.LocalBookDataSource;
import com.collegetime.theworldofbooks.EventBus.DownloadCountEvent;
import com.collegetime.theworldofbooks.EventBus.HideFabEvent;
import com.collegetime.theworldofbooks.EventBus.showTitleEvent;
import com.collegetime.theworldofbooks.ui.home.HomeFragment;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Objects;

import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private int menuClickId = -1;
    NavController navController;
    DrawerLayout drawer;
    FloatingActionButton fab;
    private AppBarConfiguration mAppBarConfiguration;
    TextView downloadedBooksCount;
    BookDataSource bookDataSource;
    SharedPreferences preferences;
    int BACK_PRESS_COUNT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        preferences = getSharedPreferences("AppData", Context.MODE_PRIVATE);

        //  SETTING VALUES FOR DOWNLOAD EXPIRY RELATED
        Common.EXPIRY_DATE = preferences.getString("validTill", "");
        Common.LAST_ONLINE = preferences.getString("lastOnline", "");

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawers();
                navController.navigate(R.id.nav_download);
                menuClickId = R.id.nav_download;
            }
        });

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_download, R.id.nav_aboutUs)
                .setDrawerLayout(drawer)
                .build();


        bookDataSource = new LocalBookDataSource(BookDatabase.getInstance(this).bookDAO());
        downloadedBooksCount = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_download));
        downloadCounts(new DownloadCountEvent(true));


        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.bringToFront();


        try {
            if (getIntent().getStringExtra("isInternetAvailable").equals("No")) {
                navController.navigate(R.id.nav_download);
                menuClickId = R.id.nav_download;
                drawer.closeDrawers();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void downloadCounts(DownloadCountEvent event) {
        if (event.isSuccess()) {
            bookDataSource.getDownloadedBooksCount().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<Integer>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onSuccess(Integer integer) {
                            downloadedBooksCount.setGravity(Gravity.CENTER_VERTICAL);

                            downloadedBooksCount.setTextColor(getResources().getColor(R.color.colorAccentOld));
                            downloadedBooksCount.setTypeface(null, Typeface.BOLD);
                            ;
                            downloadedBooksCount.setTextSize(1, 15);
                            if (integer != 0)
                                downloadedBooksCount.setText(String.valueOf(integer));
                            else
                                downloadedBooksCount.setText("");


                        }

                        @Override
                        public void onError(Throwable e) {

                        }
                    });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawer.closeDrawers();
        switch (item.getItemId()) {
            case R.id.nav_home:
                if (item.getItemId() != menuClickId)
                    navController.navigate(R.id.nav_home);
                break;

            case R.id.nav_download:
                if (item.getItemId() != menuClickId)
                    navController.navigate(R.id.nav_download);
                break;

            case R.id.nav_aboutUs:
                if (item.getItemId() != menuClickId)
                    navController.navigate(R.id.nav_aboutUs);
                break;

            case R.id.nav_favourite:
                if (item.getItemId() != menuClickId)
                    navController.navigate(R.id.nav_favourite);
                break;

            case R.id.nav_currently_reading:
                if (item.getItemId() != menuClickId)
                    navController.navigate(R.id.nav_currently_reading);
                break;


            case R.id.nav_share:
                shareAction();
                break;

            case R.id.nav_feedback:

                try {
                    SendLogcatMail();
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(HomeActivity.this, "Failed to send feedback", Toast.LENGTH_LONG).show();
                }
                break;

        }
        menuClickId = item.getItemId();
        return true;
    }


    public void shareAction() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "Tell a friend");
        String sAux = "\uD83D\uDE01 ** *PAID BOOKS GONE FREE* ** \uD83D\uDE01" + "\n Don't trust us? Download this app *WORLD OF BOOKS*  and get your " +

                "desired paid book for free \uD83D\uDC47\uD83C\uDFFD" +
                "https://play.google.com/store/apps/details?id=com.collegetime.theworldofbooks&hl=en\n";

        if (!Common.HOT_LINK_ENABLED) {
            sAux = "\uD83D\uDE01 ** *PAID BOOKS GONE FREE* ** \uD83D\uDE01" + "\n Don't trust us? Download this app *WORLD OF BOOKS*  and get your " +

                    "desired paid book for free \uD83D\uDC47\uD83C\uDFFD" +
                    "https://play.google.com/store/apps/details?id=com.collegetime.theworldofbooks&hl=en\n";
        } else {
            sAux = "\uD83D\uDE01 ** *PAID BOOKS GONE FREE* ** \uD83D\uDE01" + "\n Don't trust us? Download this app *WORLD OF BOOKS*  and get your " +

                    "desired paid book for free \uD83D\uDC47\uD83C\uDFFD" +
                    Common.HOT_LINK + "\n";
        }
        i.putExtra(Intent.EXTRA_TEXT, sAux);
        startActivity(Intent.createChooser(i, "choose one"));
    }


    public void SendLogcatMail() throws IOException {

        // save logcat in file
        File storageDir =
                getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File logfile = File.createTempFile(
                "logcat",  /* prefix */
                ".txt",         /* suffix */
                storageDir      /* directory */
        );


        try {
            Runtime.getRuntime().exec(
                    "logcat -f " + logfile.getAbsolutePath());
        } catch (IOException e) {

            e.printStackTrace();
        }
        String to[] = {"worldofbookshelp@gmail.com"};
        //send file using email
        @SuppressLint("IntentReset") Intent emailIntent = new Intent(Intent.ACTION_SEND);
        // Set type to "email"
        emailIntent.setType("vnd.android.cursor.dir/email");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        // the attachment
        Uri log = FileProvider.getUriForFile(this, "com.collegetime.theworldofbooks.fileprovider", logfile);
        emailIntent.putExtra(Intent.EXTRA_STREAM, log);
        // the mail subject

        try {
            StringBuilder builder = new StringBuilder();
            builder.append("android : ").append(Build.VERSION.RELEASE);


            Field[] fields = Build.VERSION_CODES.class.getFields();
            for (Field field : fields) {
                String fieldName = field.getName();
                int fieldValue = -1;

                try {
                    fieldValue = field.getInt(new Object());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                if (fieldValue == Build.VERSION.SDK_INT) {
                    builder.append(" : ").append(fieldName).append(" : ");
                    builder.append("sdk=").append(fieldValue);
                }
            }
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback from OS: " + builder.toString());
        } catch (Exception e) {
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback from User ");
            e.printStackTrace();
        }
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void hideFab(HideFabEvent event) {
        if (event.isSuccess()) {
            fab.setVisibility(View.GONE);
        } else {
            fab.setVisibility(View.VISIBLE);
        }
    }

    public void goToHomeFragment() {
        drawer.closeDrawers();
        navController.navigate(R.id.nav_home);
        menuClickId = R.id.nav_home;
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        try {
            FirebaseAnalytics.getInstance(getBaseContext()).logEvent("HOME_ACTIVITY_OPENED", null);
            FirebaseInAppMessaging.getInstance().triggerEvent("HOME_ACTIVITY_OPENED");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onStop();
    }

    @Override
    protected void onPause() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().removeAllStickyEvents();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);

        }
    }

    @Override
    public void onBackPressed() {
        try {
            if (Objects.requireNonNull(HomeFragment.searchList.getAdapter()).getItemCount() < Common.PROMO_BOOK_COUNT) {
                HomeFragment.parent_title_of_title.setVisibility(View.GONE);
                HomeFragment.getPromoBook();

            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            finishAffinity();
        }


        switch (Common.CURRENT_FRAGMENT) {
            case R.id.nav_aboutUs:
            case R.id.nav_download:
            case R.id.nav_favourite:
            case R.id.nav_currently_reading:
                navController.navigate(R.id.nav_home);
                menuClickId = R.id.nav_home;
                BACK_PRESS_COUNT = 0;
                drawer.closeDrawers();
                break;

            case R.id.nav_home:
                if (Common.COUNT_BACK_PRESS)
                {
                    BACK_PRESS_COUNT++;
                }
                else {
                    BACK_PRESS_COUNT = 0; // means we are changing adapter to promo books so we need to make count=0
                }

                if (BACK_PRESS_COUNT != 2) {
                    menuClickId = R.id.nav_home;
                    drawer.closeDrawers();
                    HomeFragment.parent_title_of_title.setVisibility(View.GONE);
                    HomeFragment.getPromoBook();
                    Toast.makeText(HomeActivity.this, "Press again to exit", Toast.LENGTH_LONG).show();
                } else {
                    finishAffinity();
                }
                break;


        }
    }
}
