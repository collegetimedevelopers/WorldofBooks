package com.collegetime.theworldofbooks.Database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.collegetime.theworldofbooks.Database.StarredBook.StarredBookItems;

@Database(entities ={ BookItem.class, StarredBookItems.class},version =3 ,exportSchema = false)
public abstract class BookDatabase extends RoomDatabase {

public abstract BookDAO  bookDAO();
private static BookDatabase instance;
// static   Migration migration= new Migration(1,2)
//    {
//
//        @Override
//        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("CREATE TABLE IF NOT EXISTS'StarredBooks'('BookId' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,'BookName' String,'Author' String,'Language' String,'Book_CostInCredits' String,'Year' String,'Cover_image_link' String,'Download_link' String, 'Isbn' String,'Description' String)");
//        }
//    };

public static BookDatabase getInstance(Context context)
{
    if (instance == null)
    {
       instance= Room.databaseBuilder(context,BookDatabase.class,"DownloadedBooksDB1").fallbackToDestructiveMigration().build();

    }
return instance;
}

}
