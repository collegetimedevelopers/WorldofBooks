package com.collegetime.theworldofbooks.Models;

import java.util.Base64;

public class SearchLinkData {
    private String SEARCH_DOMAIN ;
    private String SEARCH_URL;
    private String LOAD_BOOK_DETAILS_IP ;
    private String SEARCH_URL_ATTRIBUTES;

    public SearchLinkData() {
    }

    public String getSEARCH_DOMAIN() {
        return SEARCH_DOMAIN;
    }

    public void setSEARCH_DOMAIN(String SEARCH_DOMAIN) {
        this.SEARCH_DOMAIN = SEARCH_DOMAIN;
    }

    public String getSEARCH_URL() {
        return SEARCH_URL;
    }

    public void setSEARCH_URL(String SEARCH_URL) {
        this.SEARCH_URL = SEARCH_URL;
    }

    public String getLOAD_BOOK_DETAILS_IP() {
        return LOAD_BOOK_DETAILS_IP;
    }

    public void setLOAD_BOOK_DETAILS_IP(String LOAD_BOOK_DETAILS_IP) {
        this.LOAD_BOOK_DETAILS_IP = LOAD_BOOK_DETAILS_IP;
    }

    public String getSEARCH_URL_ATTRIBUTES() {
        return SEARCH_URL_ATTRIBUTES;
    }

    public void setSEARCH_URL_ATTRIBUTES(String SEARCH_URL_ATTRIBUTES) {
        this.SEARCH_URL_ATTRIBUTES = SEARCH_URL_ATTRIBUTES;
    }
}
