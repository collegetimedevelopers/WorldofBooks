package com.collegetime.theworldofbooks.ui.CurrentlyReadingBooks;

import android.content.Context;
import android.content.pm.PackageManager;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.collegetime.theworldofbooks.Database.BookDataSource;
import com.collegetime.theworldofbooks.Database.BookDatabase;
import com.collegetime.theworldofbooks.Database.BookItem;
import com.collegetime.theworldofbooks.Database.LocalBookDataSource;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CurrentlyReadingViewModel extends ViewModel {

    MutableLiveData<List<BookItem>> currentlyReadingLiveData;
    CompositeDisposable compositeDisposable;
    BookDataSource bookDataSource;
    public CurrentlyReadingViewModel()
    {
        compositeDisposable= new CompositeDisposable();
    }
    public  void  initBookDataSource(Context  context)
    {
        bookDataSource= new LocalBookDataSource(BookDatabase.getInstance(context).bookDAO());
    }

    public MutableLiveData<List<BookItem>> getCurrentlyReadingLiveData() {
        if (currentlyReadingLiveData==null)
            currentlyReadingLiveData= new MutableLiveData<>();
        getCurrentlyReadingBooks();
        return currentlyReadingLiveData;
    }

    private void getCurrentlyReadingBooks() {
        compositeDisposable.add(bookDataSource.getcurrentlyReadingBook()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bookItems -> currentlyReadingLiveData.setValue(bookItems),

                        throwable ->{
                    currentlyReadingLiveData=null;
                }));
    }


}
