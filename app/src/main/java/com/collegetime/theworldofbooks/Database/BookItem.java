package com.collegetime.theworldofbooks.Database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "Books", indices = {@Index(value = "bookName", unique = true)})
public class BookItem {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "bookId")
    private String bookId;

    @ColumnInfo(name = "bookName")
    private String bookName;

    @ColumnInfo(name = "bookImage")
    private String bookImage;

    @ColumnInfo(name = "bookPath")
    private String bookPath;

    @ColumnInfo(name = "bookExtension")
    private String bookExt;

    @ColumnInfo(name = "favourateBook")
    private boolean favourateBook;

    @ColumnInfo(name = "currentlyReadingBook")
    private boolean currentlyReadingBook;

    @ColumnInfo(name = "completed_currently_ReadingBook")
    private boolean completed_currently_ReadingBook;

    @ColumnInfo(name = "download_date")
    private String download_date;

    @ColumnInfo(name = "download_expiry_date")
    private String download_expiry_date;


    public String getDownload_date() {
        return download_date;
    }

    public void setDownload_date(String download_date) {
        this.download_date = download_date;
    }

    public String getDownload_expiry_date() {
        return download_expiry_date;
    }

    public void setDownload_expiry_date(String download_expiry_date) {
        this.download_expiry_date = download_expiry_date;
    }


    public BookItem(String bookId, String bookName, String bookImage, String bookPath, String bookExt, boolean favourateBook, boolean currentlyReadingBook, boolean completed_currently_ReadingBook) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookImage = bookImage;
        this.bookPath = bookPath;
        this.bookExt = bookExt;
        this.favourateBook = favourateBook;
        this.currentlyReadingBook = currentlyReadingBook;
        this.completed_currently_ReadingBook = completed_currently_ReadingBook;
    }

    public BookItem() {

    }

    @NonNull
    public String getBookId() {
        return bookId;
    }

    public void setBookId(@NonNull String bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }


    public String getBookPath() {
        return bookPath;
    }

    public void setBookPath(String bookPath) {
        this.bookPath = bookPath;
    }

    public String getBookExt() {
        return bookExt;
    }

    public void setBookExt(String bookExt) {
        this.bookExt = bookExt;
    }

    public boolean isFavourateBook() {
        return favourateBook;
    }

    public void setFavourateBook(boolean favourateBook) {
        this.favourateBook = favourateBook;
    }

    public boolean isCurrentlyReadingBook() {
        return currentlyReadingBook;
    }

    public void setCurrentlyReadingBook(boolean currentlyReadingBook) {
        this.currentlyReadingBook = currentlyReadingBook;
    }

    public boolean isCompleted_currently_ReadingBook() {
        return completed_currently_ReadingBook;
    }

    public void setCompleted_currently_ReadingBook(boolean completed_currently_ReadingBook) {
        this.completed_currently_ReadingBook = completed_currently_ReadingBook;
    }
}
