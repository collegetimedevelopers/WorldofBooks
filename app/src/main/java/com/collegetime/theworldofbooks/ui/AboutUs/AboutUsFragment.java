package com.collegetime.theworldofbooks.ui.AboutUs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.EventBus.HideFabEvent;
import com.collegetime.theworldofbooks.R;

import org.greenrobot.eventbus.EventBus;

public class AboutUsFragment extends Fragment {

    private AboutUsViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(AboutUsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_about_us, container, false);

        EventBus.getDefault().postSticky(new HideFabEvent(true));
        slideshowViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        WebView webView = root.findViewById(R.id.about_us);
        webView.loadUrl("file:///android_asset/About us Page.html");
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        Common.CURRENT_FRAGMENT=R.id.nav_aboutUs;

    }
}
