package com.collegetime.theworldofbooks.EventBus;

public class DownloadCountEvent {
    boolean success;

    public DownloadCountEvent(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
