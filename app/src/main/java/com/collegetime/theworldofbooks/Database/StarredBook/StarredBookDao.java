package com.collegetime.theworldofbooks.Database.StarredBook;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface StarredBookDao {

    @Query("SELECT * FROM StarredBooks")
    Flowable<List<StarredBookItems>> getAllStarredBooks();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertStarredBook(StarredBookItems...starredBookItems);

    @Delete
    Single<Integer> deleteStarredBook(StarredBookItems starredBookItems);

}
