package com.collegetime.theworldofbooks.ui.FavourateBooks;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.collegetime.theworldofbooks.Database.BookDataSource;
import com.collegetime.theworldofbooks.Database.BookDatabase;
import com.collegetime.theworldofbooks.Database.BookItem;
import com.collegetime.theworldofbooks.Database.LocalBookDataSource;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class FavouriteViewModel extends ViewModel {


    private MutableLiveData<List<BookItem>> favouriteBooksLiveData;
    CompositeDisposable compositeDisposable;
    BookDataSource bookDataSource;

    public FavouriteViewModel() {
     compositeDisposable =new CompositeDisposable();
    }

    public void initFavouriteBookDataSource(Context context)
    {
        bookDataSource= new LocalBookDataSource(BookDatabase.getInstance(context).bookDAO());
    }

    public MutableLiveData<List<BookItem>> getFavouriteBooksLiveData() {
        if (favouriteBooksLiveData==null)

            favouriteBooksLiveData= new MutableLiveData<>();

        getAllFavouriteBooks();
        return favouriteBooksLiveData;
    }

    private void getAllFavouriteBooks() {

        compositeDisposable.add(bookDataSource.getFavouriteBooks().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<BookItem>>() {
            @Override
            public void accept(List<BookItem> bookItems) throws Exception {
                favouriteBooksLiveData.setValue(bookItems);
            }
        },throwable -> {
            favouriteBooksLiveData.setValue(null);
        }));

    }

}
