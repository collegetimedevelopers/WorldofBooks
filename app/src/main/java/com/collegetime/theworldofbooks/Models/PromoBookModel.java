package com.collegetime.theworldofbooks.Models;

public class PromoBookModel {
   private String title,price,description,url,searchKey;

    public PromoBookModel(String title, String price, String description, String url) {
        this.title = title;
        this.price = price;
        this.description = description;
        this.url = url;
    }

    public PromoBookModel() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }
}
