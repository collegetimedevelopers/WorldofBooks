package com.collegetime.theworldofbooks.Executors;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.HomeActivity;
import com.collegetime.theworldofbooks.LoadBookDetail;
import com.collegetime.theworldofbooks.ui.home.HomeFragment;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;

import io.reactivex.Completable;

public class LoadBook extends AsyncTask<Void,Void,Document> {


    @SuppressLint("StaticFieldLeak")
    Context context;
    String link;
    ProgressDialog dialog ;
    Document doc;
    boolean  connected = false;
    Bitmap bitmap;
    InputStream input;
    String title,author,year,publisher,isbn,description;
    public LoadBook(Context context, String link){
        this.context = context;
        this.link = link;
        dialog = new ProgressDialog(context);
        dialog.setMessage("Loading...");

        dialog.setCancelable(false);
    }
    public  boolean isConnectedToServer() {
        try {
            URL myUrl = new URL(Common.LOAD_BOOK_DETAILS_IP);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(4000);
            connection.connect();
            return true;
        } catch (Exception e) {
            // Handle your exceptions
            return false;
        }
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog.show();
        InetAddress ip = null;


        if(isConnectedToServer())
        {
            connected = true;
            Log.v("CONN", "Connected to " + Common.LOAD_BOOK_DETAILS_IP);
        }

    }


    @Override
    protected Document doInBackground(Void... voids) {
        if(connected) {
            int tries = 0;
//            Log.v("URL", link);
            while (tries<3) {
                try {
                    doc = Jsoup.connect(link).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .timeout(10000).get();

                } catch (IOException e) {
                    e.printStackTrace();

                }
                tries++;

            }
            return doc;
        }
        else
        {
            return null;
        }

    }
    @Override
    protected void onPostExecute(Document document) {
        super.onPostExecute(document);

        if(document!=null)
        {

            String url = null;
            try {
                Element image = document.select("img").first();
                url = image.absUrl("src");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                 input = new java.net.URL(url).openStream();
            } catch (IOException e) {
                e.printStackTrace();
                dialog.dismiss();
            }
            if(input!=null)
            {
                bitmap = BitmapFactory.decodeStream(input);
                if(Common.ShowCover) {
                    LoadBookDetail.cover.setImageBitmap(bitmap);
                }
                else
                {
                    LoadBookDetail.cover.setVisibility(View.GONE);
                }

            }

            dialog.dismiss();
            Element h1 =  document.selectFirst("h1");
            Elements p = document.select("p");
            Elements div = document.select("div");
            try {
                LoadBookDetail.downloadLink = document.select("a").first().attr("href").trim();
            } catch (Exception e) {
                e.printStackTrace();
            }


            if(p.size()>4)
            {
                try {
                    author = p.get(0).text();
                    author = author.replace("Author(s): ","").trim();
                    year = p.get(2).text().trim();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String[] s = year.split("Year:");
                if(s[0].isEmpty())
                {
                    isbn = p.get(1).text().trim();
                    isbn= isbn.replace("ISBN:","").trim();
                    LoadBookDetail.author.setText(author);
                    LoadBookDetail.title.setText(h1.text().trim());
                    LoadBookDetail.isbn.setText(isbn);

                    LoadBookDetail.layout.setVisibility(View.VISIBLE);
                }
                else {
                    try {
                        year = s[1].trim();
                        publisher = s[0].trim();
                        publisher = publisher.replace("Publisher:", "").trim();
                        publisher = publisher.replace(",", "");
                        isbn = p.get(3).text().trim();

                        isbn = isbn.replace("ISBN:", "").trim();
                        LoadBookDetail.author.setText(author);
                        LoadBookDetail.year.setText(year);
                        LoadBookDetail.title.setText(h1.text().trim());

                        LoadBookDetail.publisher.setText(publisher);
                        LoadBookDetail.isbn.setText(isbn);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    LoadBookDetail.layout.setVisibility(View.VISIBLE);
                }
                try {
                    description = div.get(1).text();
                    description =  description .replace("Description:","");
                    LoadBookDetail.description.setText(description);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            else {
                String[] s = new String[0];
                try {
                    author = p.get(0).text();
                    author = author.replace("Author(s): ", "").trim();
                    year = p.get(1).text().trim();
                    s = year.split("Year:");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(s[0].isEmpty())
                {
                    isbn = p.get(1).text().trim();
                    isbn= isbn.replace("ISBN:","").trim();
                    LoadBookDetail.author.setText(author);
                    LoadBookDetail.title.setText(h1.text().trim());
                    LoadBookDetail.isbn.setText(isbn);
                    LoadBookDetail.layout.setVisibility(View.VISIBLE);
                }
                else {
                    try {
                        year = s[1].trim();
                        publisher = s[0].trim();
                        publisher = publisher.replace("Publisher:", "").trim();
                        publisher = publisher.replace(",", "");
                        isbn = p.get(2).text().trim();
                        isbn = isbn.replace("ISBN:", "").trim();
                        LoadBookDetail.author.setText(author);
                        LoadBookDetail.year.setText(year);
                        LoadBookDetail.title.setText(h1.text().trim());

                        LoadBookDetail.publisher.setText(publisher);
                        LoadBookDetail.isbn.setText(isbn);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    LoadBookDetail.layout.setVisibility(View.VISIBLE);
                }
                try {
                    description = div.get(1).text();
                    description =  description .replace("Description:","");
                    LoadBookDetail.description.setText(description);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }


        else
        {
            dialog.dismiss();
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Some error occurred, please GO BACK  and try after some time & make sure that you have an active internet connection.").setTitle("Connectivity");
            builder.setCancelable(true);
            builder.show();

        }

    }

}