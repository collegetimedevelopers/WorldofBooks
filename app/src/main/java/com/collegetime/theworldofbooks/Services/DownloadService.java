package com.collegetime.theworldofbooks.Services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.airbnb.lottie.L;
import com.collegetime.theworldofbooks.BroadcastReciever.NotificationReceiver;
import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.Database.BookDAO;
import com.collegetime.theworldofbooks.Database.BookDataSource;
import com.collegetime.theworldofbooks.Database.BookDatabase;
import com.collegetime.theworldofbooks.Database.BookItem;

import com.collegetime.theworldofbooks.Database.LocalBookDataSource;
import com.collegetime.theworldofbooks.EventBus.DownloadCountEvent;
import com.collegetime.theworldofbooks.HomeActivity;
import com.collegetime.theworldofbooks.LoadBookDetail;
import com.collegetime.theworldofbooks.R;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class DownloadService extends Service {
    NotificationManagerCompat mNotification;
    NotificationCompat.Builder builder;
    String CHANNEL_ID = "Download";
    File SDCardRoot;
    String ret;
    PowerManager.WakeLock mWakeLock;
    NotificationChannel channel;
    BookDAO bookDAO;
    String url, title, extension;
    int filelength;
    CompositeDisposable compositeDisposable;
    BookDataSource bookDataSource;
    BookDatabase bookDatabase;
    Downloader downloader;

    @Override
    public void onCreate() {
        bookDatabase=BookDatabase.getInstance(DownloadService.this);
        compositeDisposable= new CompositeDisposable();
        bookDataSource= new LocalBookDataSource(BookDatabase.getInstance(DownloadService.this).bookDAO());
        downloader = new Downloader();
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        assert pm != null;
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                getClass().getName());
        mWakeLock.acquire(15*60*1000L /*15 minutes*/);

        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        url = intent.getStringExtra("link");
        extension = intent.getStringExtra("extension");
        title = intent.getStringExtra("title");
        downloader.execute(url);

        return START_STICKY;

    }

    @Override
    public void onDestroy() {
        downloader.cancel(false);
        if(mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        stopSelf();
        super.onDestroy();

    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restart = new Intent(getApplicationContext(),this.getClass());
        restart.setPackage(getPackageName());
        startService(restart);
        super.onTaskRemoved(rootIntent);
    }


    class Downloader extends AsyncTask<String, Integer, String> {


        @Override
        protected String doInBackground(String... strings) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection) url.openConnection();
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    ret = "Server returned HTTP " + connection.getResponseCode() + " " + connection.getResponseMessage();
                    Log.v("CONN", ret);
                    return ret;

                }
              File rootStorage=new File(String.valueOf(getApplicationContext().getExternalFilesDir("storage/emulated/0/WorldOfBooks")));
                if (!rootStorage.exists())
                {
                    rootStorage.mkdirs();
                }

                File downloadedFile= new File(rootStorage.getAbsolutePath()+"/"+title+"."+extension);

                if (!downloadedFile.exists())
                {
                    downloadedFile.createNewFile();
                }

                  filelength = connection.getContentLength();
                input = connection.getInputStream();
//                File rootStorage= new File(Environment.getExternalStoragePublicDirectory(Environment.),"/sdcard/WorldOfBooks/"+title+"."+extension);

//
                output = new FileOutputStream(downloadedFile);
                Common.BOOK_NAME=title;
                Common.BOOK_PATH=rootStorage.getAbsolutePath()+"/"+title+"."+extension;

                byte[] data = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (filelength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / filelength));
                    output.write(data, 0, count);
                }
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            createNotificationChannel();
            Intent i = new Intent(DownloadService.this, NotificationReceiver.class);
            i.putExtra("id",101);
            PendingIntent resultIntent = PendingIntent.getBroadcast(getBaseContext(),0,i,PendingIntent. FLAG_UPDATE_CURRENT);
            mNotification = NotificationManagerCompat.from(getApplicationContext());
            builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setContentText("Starting Download, this may take a while").setContentTitle("File Download").setSmallIcon(R.drawable.ic_file_download_black_24dp);
            builder.setPriority(Notification.PRIORITY_LOW);
            builder.setContentIntent(resultIntent);
            builder.addAction(R.drawable.ic_cancel_black_24dp,"Cancel",resultIntent);
            mNotification.notify(101, builder.build());


        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);


            if (progress[0] >= 100) {
                Intent i= new Intent(DownloadService.this, HomeActivity.class);
                i.putExtra("isInternetAvailable","No");
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadService.this);
                stackBuilder.addNextIntentWithParentStack(i);
                PendingIntent resultIntent  = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(resultIntent);
                builder.setContentText("File Downloaded, tap to view").setProgress(0, 0, false);
                mNotification.notify(101, builder.build());
            }
            Intent i = new Intent(getBaseContext(), NotificationReceiver.class);
            i.putExtra("id",101);
            PendingIntent resultIntent = PendingIntent.getBroadcast(getBaseContext(),0,i,PendingIntent. FLAG_UPDATE_CURRENT);
            builder.setContentText("Downloaded "+progress[0]+"%");
            builder.setContentIntent(resultIntent);
            builder.setProgress(100, progress[0], false);
            mNotification.notify(101, builder.build());

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        protected void onPostExecute(String result) {
            if (result != null) {
                Intent i= new Intent(getBaseContext(), HomeActivity.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadService.this);
                stackBuilder.addNextIntentWithParentStack(i);
                PendingIntent resultIntent  = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                { channel.setImportance(NotificationManager.IMPORTANCE_HIGH);}
                builder.setContentIntent(resultIntent);
                Toast.makeText(getApplicationContext(), "Download error: " + result, Toast.LENGTH_LONG).show();
                builder.setContentText("Download Error").setPriority(Notification.PRIORITY_MAX);
                mNotification.notify(101,builder.build());
                Snackbar snackbar = Snackbar.make(LoadBookDetail.layout, "Download failed,some error occurred. Tap to retry", Snackbar.LENGTH_LONG);
                snackbar.show();
                mWakeLock.release();
                stopSelf();

            } else {

                BookItem bookItem= new BookItem();
                    bookItem.setBookPath(Common.BOOK_PATH);
                    bookItem.setBookName(title);
                    bookItem.setCurrentlyReadingBook(false);
                    bookItem.setFavourateBook(false);
                    bookItem.setBookImage(Common.BOOK_IMAGE);
                    bookItem.setBookExt(extension);
                    bookItem.setCompleted_currently_ReadingBook(false);
                FirebaseAnalytics.getInstance(getBaseContext()).logEvent("BOOK_SUCCESSFULLY_DOWNLOADED",null);
                FirebaseInAppMessaging.getInstance().triggerEvent("BOOK_SUCCESSFULLY_DOWNLOADED");

                Log.d("Download","...........BOOK_IMAGE.........."+Common.BOOK_IMAGE);

                compositeDisposable.add(bookDataSource.InsertBook(bookItem).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(()->{


                    try {
                        FirebaseAnalytics.getInstance(getBaseContext()).logEvent("BOOK_SUCCESSFULLY_DOWNLOADED",null);
                        FirebaseInAppMessaging.getInstance().triggerEvent("BOOK_SUCCESSFULLY_DOWNLOADED");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                },throwable -> {
                    Log.e("INSERTION ERROR ","Failed to insert in downloaded books database -> DownloadService ->postExecute");
                    Toast.makeText(DownloadService.this,"Failed to add to downloads"+throwable.getMessage(),Toast.LENGTH_LONG).show();

                }));

                EventBus.getDefault().postSticky(new DownloadCountEvent(true));

              Intent i= new Intent(DownloadService.this, HomeActivity.class);
                i.putExtra("isInternetAvailable","No");
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(DownloadService.this);
                stackBuilder.addNextIntentWithParentStack(i);
                PendingIntent resultIntent  = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                { channel.setImportance(NotificationManager.IMPORTANCE_HIGH);}
                builder.setContentIntent(resultIntent);
                builder.setPriority(NotificationCompat.PRIORITY_MAX);
                builder.setContentText("File Downloaded, tap to view").setProgress(0, 0, false).setPriority(Notification.PRIORITY_MAX);
                mNotification.notify(101, builder.build());
                Snackbar snackbar = Snackbar.make(LoadBookDetail.layout, "Book Downloaded", Snackbar.LENGTH_LONG);
                snackbar.show();
                mWakeLock.release();
                stopSelf();
            }
        }

        private void createNotificationChannel() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                 channel = new NotificationChannel(CHANNEL_ID, "Download", NotificationManager.IMPORTANCE_LOW);
                channel.setDescription("Download");
                NotificationManager manager = getApplicationContext().getSystemService(NotificationManager.class);
                assert manager != null;
                manager.createNotificationChannel(channel);
            }
        }
    }
}


