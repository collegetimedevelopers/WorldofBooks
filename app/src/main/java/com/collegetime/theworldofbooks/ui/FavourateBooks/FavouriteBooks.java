package com.collegetime.theworldofbooks.ui.FavourateBooks;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.collegetime.theworldofbooks.Adapters.DownloadedBooksAdapter;
import com.collegetime.theworldofbooks.Adapters.FavouriteBooksAdapter;
import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.Common.SwipeHelper;
import com.collegetime.theworldofbooks.Database.BookDataSource;
import com.collegetime.theworldofbooks.Database.BookDatabase;
import com.collegetime.theworldofbooks.Database.BookItem;
import com.collegetime.theworldofbooks.Database.LocalBookDataSource;
import com.collegetime.theworldofbooks.EventBus.DownloadCountEvent;
import com.collegetime.theworldofbooks.EventBus.HideFabEvent;
import com.collegetime.theworldofbooks.R;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.ParseException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.CompletableObserver;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class FavouriteBooks extends Fragment {

    @BindView(R.id.download_ebook)
    TextView downloadEbook;
    @BindView(R.id.favourite_book_recycler_view)
    RecyclerView favouriteBookRecyclerView;
    @BindView(R.id.empty_favourite)
    TextView emptyFavourite;
    @BindView(R.id.downloaded_book_layout)
    RelativeLayout downloadedBookLayout;
    private FavouriteViewModel favouriteViewModel;
    FavouriteBooksAdapter favouriteBooksAdapter;
    @BindView(R.id.text_favourite_info)
    TextView text_favourite_info;
    BookDataSource bookDataSource;
    Unbinder unbinder;

    SharedPreferences preferences;

    public static FavouriteBooks newInstance() {
        return new FavouriteBooks();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        favouriteViewModel = ViewModelProviders.of(this).get(FavouriteViewModel.class);
        View root = inflater.inflate(R.layout.favourate_fragment, container, false);
        favouriteViewModel.initFavouriteBookDataSource(getContext());


            favouriteViewModel.getFavouriteBooksLiveData().observe(this, bookItems -> {


                        if (null == bookItems || bookItems.isEmpty()) {
                            favouriteBookRecyclerView.setVisibility(View.GONE);
                            text_favourite_info.setVisibility(View.GONE);
                            downloadEbook.setVisibility(View.GONE);
                            emptyFavourite.setVisibility(View.VISIBLE);


                        } else {
                            favouriteBookRecyclerView.setVisibility(View.VISIBLE);
                            text_favourite_info.setVisibility(View.VISIBLE);
                            downloadEbook.setVisibility(View.VISIBLE);
                            emptyFavourite.setVisibility(View.GONE);

                            favouriteBooksAdapter = new FavouriteBooksAdapter(getContext(), bookItems, getActivity());
                            if (appInstalledOrNot("com.github.collegetime.bookreader")) {

                                downloadEbook.setVisibility(View.GONE);
                                text_favourite_info.setVisibility(View.GONE);
                            }
                            favouriteBookRecyclerView.setAdapter(favouriteBooksAdapter);
                        }
                    }

            );

            unbinder = ButterKnife.bind(this, root);

            initViews();


        return root;
    }


    private void initViews() {
        downloadEbook.setMovementMethod(LinkMovementMethod.getInstance());

        bookDataSource = new LocalBookDataSource(BookDatabase.getInstance(getContext()).bookDAO());
        favouriteBookRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        favouriteBookRecyclerView.setLayoutManager(linearLayoutManager);
        favouriteBookRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), linearLayoutManager.getOrientation()));

// USED     F O R       D E  L E T E     B U T T O N
        SwipeHelper swipeHelper = new SwipeHelper(getContext(), favouriteBookRecyclerView, 300) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(getContext(), "DELETE", 35, 0, Color.parseColor("#FF0000"), new UnderlayButtonClickListener() {
                    @Override
                    public void onClick(int pos) {


                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setCancelable(false);
                        builder.setTitle("Delete Book");
                        builder.setMessage("Sure to Delete this book?");
                        builder.setPositiveButton("Yes, Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                BookItem bookItem = favouriteBooksAdapter.getItemAtPosition(pos);

                                bookDataSource.deleteBookItem(bookItem).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new SingleObserver<Integer>() {
                                            @Override
                                            public void onSubscribe(Disposable d) {

                                            }

                                            @Override
                                            public void onSuccess(Integer integer) {
                                                favouriteBooksAdapter.notifyDataSetChanged();
                                                // DELETING BOOK FILE FROM STORAGE

                                                try {

                                                    File bookFile = new File(bookItem.getBookPath());
                                                    Log.d("Path", "////////path........" + bookFile);
                                                    if (bookFile.exists()) {
                                                        if (bookFile.delete()) {
                                                            Log.d("Image", "DELETED BOOK FILE//////////");
                                                        }

                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Log.e("Image", "Failed to DELETED BOOK FILE//////////");

                                                }

                                                // DELETING  IMAGE  FROM STORAGE
                                                try {
                                                    File coverImageFile = new File(bookItem.getBookImage());
                                                    if (coverImageFile.exists()) {
                                                        if (coverImageFile.delete()) {
                                                            Log.d("Image", "DELETED IMAGE//////////");
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Log.e("Image", "Failed to DELETED IMAGE//////////");
                                                }

                                                EventBus.getDefault().postSticky(new DownloadCountEvent(true));
                                                Toast.makeText(getContext(), "Book Deleted  ", Toast.LENGTH_LONG).show();

                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                                Toast.makeText(getContext(), "Failed to delete book, Please try again.", Toast.LENGTH_LONG).show();
                                            }
                                        });
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Toast.makeText(getContext(), "You saved your book \uD83D\uDE1C ", Toast.LENGTH_SHORT).show();

                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }));
//  BUTTON TO       R E M O V E     FROM   F

                underlayButtons.add(new SwipeHelper.UnderlayButton(getContext(), "Remove From" + "\n" +
                        " Favourite", 30, R.drawable.remove_from_favourite, Color.parseColor("#560027"), new UnderlayButtonClickListener() {
                    @Override
                    public void onClick(int pos) {


                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setCancelable(false);
                        builder.setTitle("Attention");
                        builder.setMessage("Sure to Remove this book from favourite books ?");
                        builder.setPositiveButton("Yes, Remove", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                BookItem bookItem = favouriteBooksAdapter.getItemAtPosition(pos);
                                bookItem.setFavourateBook(false);

                                bookDataSource.UpdateBook(bookItem).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new CompletableObserver() {
                                            @Override
                                            public void onSubscribe(Disposable d) {

                                            }

                                            @Override
                                            public void onComplete() {

                                                Toast.makeText(getContext(), "Removed From favourites", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onError(Throwable e) {
                                                Toast.makeText(getContext(), "Some error occurred while removing From favourites", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                //  Toast.makeText(getContext(),"You saved your book \uD83D\uDE1C ",Toast.LENGTH_SHORT).show();

                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }));
            }


        };

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().postSticky(new HideFabEvent(true));
        Common.CURRENT_FRAGMENT = R.id.nav_favourite;

        try {
            FirebaseAnalytics.getInstance(getContext()).logEvent("FAVOURITE_FRAGMENT_OPENED", null);
            FirebaseInAppMessaging.getInstance().triggerEvent("FAVOURITE_FRAGMENT_OPENED");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager packageManager = requireContext().getPackageManager();

        try {

            packageManager.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;

        } catch (Exception e) {

        }
        return false;
    }


    private void showExpiredDialog() throws ParseException {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.WarningMessageAlertDialogStyle);
        builder.setCancelable(false);
        builder.setMessage("Your Downloads have been expired because you were last online on " + Common.getLastOnline() + " on this app, you need to come online at least  once in a week. Please bring the app online to refresh the downloads and continue reading again.");
        builder.setTitle("ATTENTION");

        builder.setPositiveButton("GOT IT ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                preferences = getContext().getSharedPreferences("AppData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("showAds", "true");
                editor.apply();

                requireActivity().moveTaskToBack(true);
                requireActivity().finishAffinity();

            }
        });

        AlertDialog messageDialog = builder.create();
        //to set color to button without using style
        messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                messageDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(android.R.color.holo_green_light));
            }
        });
        messageDialog.show();
    }

}
