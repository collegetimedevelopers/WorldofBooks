package com.collegetime.theworldofbooks;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.WindowManager;

import com.airbnb.lottie.LottieAnimationView;
import com.collegetime.theworldofbooks.Adapters.PromoBookAdapter;
import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.Models.MessageModel;
import com.collegetime.theworldofbooks.Models.Permissions;
import com.collegetime.theworldofbooks.Models.PromoBookModel;
import com.collegetime.theworldofbooks.Models.SearchLinkData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyStore;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class StartupActivity extends AppCompatActivity {
    private static final int REQUEST_ENABLE_INTERNET = 7070;
    String ver;
    LottieAnimationView loading;
    Handler mHandler;
    MessageModel messageModel;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Permissions permissions;
    FirebaseRemoteConfig remoteConfig;
    boolean isNextActivityStarted=false;

    @SuppressLint("CommitPrefEdits")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES);
        setContentView(R.layout.activity_startup);
        System.out.println("-------------------------ON CREATE-------------");

        messageModel = new MessageModel();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        remoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3)
                .build();
        remoteConfig.setConfigSettingsAsync(configSettings);

        preferences = getSharedPreferences("AppData", Context.MODE_PRIVATE);

        editor = preferences.edit();
        new background().execute();
        loading = findViewById(R.id.loading);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                loading.setSpeed(1.0f);

            }
        });

        //HANDLER FOR UI THREAD OPERATIONS [DISPLAY DIALOGS]
        mHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                showNoInternetDialog();
            }
        };

    }

    void workerThread() {
        // And this is how you call it from the worker thread:
        Message message = mHandler.obtainMessage();
        message.sendToTarget();
    }

    // BACKGROUND TASKS AS PER FLOWCHART
    class background extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            if (Common.isConnected(getApplicationContext())) {

//                remoteConfig.setDefaultsAsync(R.xml.remote_config);
                remoteConfig.fetchAndActivate().addOnCompleteListener(new OnCompleteListener<Boolean>() {
                    @Override
                    public void onComplete(@NonNull Task<Boolean> task) {
                        if (task.isSuccessful()) {
                            System.out.println("------------REMOTE CONFIG UPDATED----------------------:" + task.getResult());
                            remoteConfig.activate();
                            setConfig();
                        }
                    }
                });
            } else {

                workerThread();
            }


            return null;
        }
    }

    protected void getVersion() {

        try {

            ver = remoteConfig.getString("VERSION");
        } catch (Exception e) {
            //  IN CASE IF WE FAILED TO GET VALUE FROM REMOTE_CONFIG , IN THIS CASE USER IS ALLOWED TO USE APP BECAUSE VERSION IS SET TO CURRENT VERSION OF APP

            try {
                PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
                ver = info.versionName;

            } catch (PackageManager.NameNotFoundException e2) {
                e2.printStackTrace();
            }
        }


        if (null != ver) {
            compareVersion(ver);

        } else {
            if (!isNextActivityStarted) {
                Intent i = new Intent(StartupActivity.this, HomeActivity.class);
                startActivity(i);
                isNextActivityStarted=true;
                finish();
            }
        }
    }

    private void compareVersion(String version) {
        // COMPARE VERSION FROM DEVICE WITH FIREBASE VERSION
        String versionName = null;
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = info.versionName;
            //Log.d("VER", "Version from device"+versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        assert versionName != null;

        if (!versionName.isEmpty()) {

            if (!versionName.equals(version)) {
                // NEW VERSION FOUND CREATE DIALOG & SHOW TO USER

                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.UpdateAlertDialogStyle);
                builder.setTitle("Update");
                builder.setCancelable(false);
                builder.setMessage(R.string.updateMessage);
                builder.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //PLAY STORE INTENT
                        if (!Common.HOT_LINK_ENABLED)
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" +"com.collegetime.theworldofbooks" )));
                        else
                        {
                            // TO DOWNLOAD APP FROM THE HOT LINK
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Common.HOT_LINK)));
                        }
                          finishAffinity();

                    }
                });
                builder.setNegativeButton("LEAVE APP", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //DOWNLOADS ACTIVITY INTENT
                        finishAffinity();
                    }
                });

                AlertDialog updateDialog = builder.create();
                 updateDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        updateDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                    }
                });
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading.pauseAnimation();
                        updateDialog.show();
                    }
                });



            } else {
                getMessage();
            }
        }


    }

    private void showNoInternetDialog() {
        // NO INTERNET DIALOG DISPLAY TO USER
        AlertDialog.Builder builder = new AlertDialog.Builder(StartupActivity.this, R.style.NoInternetAlertDialogStyle);
        builder.setCancelable(false);
        @SuppressLint("InflateParams") View itemView = LayoutInflater.from(this).inflate(R.layout.layout_no_internet, null);
        builder.setPositiveButton("SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //SETTINGS INTENT

                startActivityForResult(new Intent(Settings.ACTION_WIRELESS_SETTINGS), REQUEST_ENABLE_INTERNET);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading.resumeAnimation();

                    }
                });
                dialog.dismiss();

            }
        });
        builder.setNegativeButton("DOWNLOADS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //DOWNLOADS ACTIVITY INTENT

                Intent i = new Intent(StartupActivity.this, HomeActivity.class);
                i.putExtra("isInternetAvailable", "No");
                startActivity(i);
                finish();
                //loading.resumeAnimation();
            }
        });

        AlertDialog noInternetDialog = builder.create();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loading.pauseAnimation();
            }
        });

        noInternetDialog.setView(itemView);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                noInternetDialog.show();
            }
        });

    }

    private void getMessage() {


        try {
            messageModel.setMessage(remoteConfig.getString("MESSAGE"));
            messageModel.setRecurring(remoteConfig.getBoolean("RECURRING"));

        } catch (Exception e) {
            e.printStackTrace();

        }

        //   getPromoBooks();

        if (!messageModel.getMessage().equals("null")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showMessage();
                }
            });

        } else {
            FirebaseDatabase.getInstance().goOffline();
            if (!isNextActivityStarted) {
                Intent i = new Intent(StartupActivity.this, HomeActivity.class);
                startActivity(i);
                isNextActivityStarted=true;
                finish();
            }
        }


    }

    private void showMessage() {
        // DEFAULT VALUE SHOULD BE \n TO BOTH SERVER AND CLIENT TO FUNCTION THE LOGIC PROPERLY
        String lastMessage = preferences.getString("lastMsg", "\n");
        if (messageModel.isRecurring()) {
            // DISPLAY MESSAGE
            AlertDialog.Builder builder = new AlertDialog.Builder(StartupActivity.this, R.style.MessageAlertDialogStyle);
            builder.setCancelable(false);
            builder.setMessage(messageModel.getMessage());
            builder.setTitle("Message");

            builder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //HOME ACTIVITY INTENT

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loading.resumeAnimation();
                        }
                    });

                    FirebaseDatabase.getInstance().goOffline();
                    if (!isNextActivityStarted) {
                    Intent i = new Intent(StartupActivity.this, HomeActivity.class);
                    isNextActivityStarted=true;
                    startActivity(i);
                    finish();
                    }
                }
            });


            AlertDialog messageDialog = builder.create();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loading.pauseAnimation();
                }
            });

            messageDialog.show();
        } else {
            if (!lastMessage.equals(messageModel.getMessage())) {
                //DISPLAY MESSAGE & SAVE TO SHARED PREFERENCES
                AlertDialog.Builder builder = new AlertDialog.Builder(StartupActivity.this, R.style.MessageAlertDialogStyle);
                builder.setCancelable(false);
                builder.setMessage(messageModel.getMessage());
                builder.setTitle("Message");

                builder.setPositiveButton("CONTINUE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // SAVING MSG & HOME ACTIVITY INTENT
                        editor.putString("lastMsg", messageModel.getMessage());
                        editor.apply();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loading.resumeAnimation();
                            }
                        });
                        FirebaseDatabase.getInstance().goOffline();
                        if (!isNextActivityStarted) {
                        Intent i = new Intent(StartupActivity.this, HomeActivity.class);
                        startActivity(i);
                        isNextActivityStarted=true;
                        finish();
                    }
                    }
                });


                AlertDialog messageDialog = builder.create();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loading.pauseAnimation();
                    }
                });

                messageDialog.show();
            } else {

                FirebaseDatabase.getInstance().goOffline();
                if (!isNextActivityStarted) {
                Intent i = new Intent(StartupActivity.this, HomeActivity.class);
                startActivity(i);
                isNextActivityStarted=true;
                finish();
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {


            case REQUEST_ENABLE_INTERNET:
                if (Common.isConnected(StartupActivity.this)) {
                    setConfig();
                 //   getPromoBooks();
//                    if (!isNextActivityStarted) {
//                    startActivity(new Intent(StartupActivity.this, HomeActivity.class));
//
//                    }
                    finish();
                } else {
                    workerThread();

                }


        }
    }

    private void setConfig() {
        Common.ShowCover = remoteConfig.getBoolean("SHOW_COVER");
        Common.ShowPromo = remoteConfig.getBoolean("SHOW_PROMO");
        Common.AllowDownload = remoteConfig.getBoolean("ALLOW_DOWNLOAD");
        Common.SEARCH_DOMAIN = remoteConfig.getString("SEARCH_DOMAIN");
        Common.SEARCH_URL = remoteConfig.getString("SEARCH_URL");
        Common.LOAD_BOOK_DETAILS_IP = remoteConfig.getString("LOAD_BOOK_DETAILS_IP");
        Common.SEARCH_URL_ATTRIBUTES = remoteConfig.getString("SEARCH_URL_ATTRIBUTES");
        Common.HOT_LINK_ENABLED=remoteConfig.getBoolean("ENABLE_HOT_LINK");
        Common.HOT_LINK=remoteConfig.getString("HOT_LINK");
        Common.BOOK_EXPIRY_OFFER=remoteConfig.getBoolean("BOOK_EXPIRY_CRITERIA");

        if (Common.ShowPromo) {
            getPromoBooks();
            getVersion();
        } else {
            getVersion();
        }


    }

    private void getPromoBooks() {
        // GETTING LIST OF PROMO BOOKS FROM SEVERS
        PromoBookModel promoBookModel;
        ArrayList<PromoBookModel> bookModels = new ArrayList<>();

        try {
            JSONObject promo_books = new JSONObject(remoteConfig.getString("PROMO_BOOKS"));
            JSONArray books_array = promo_books.getJSONArray("PromoBooks");
            Gson g = new Gson();
            for (int i = 0; i < books_array.length(); i++) {
                promoBookModel = new PromoBookModel();
                promoBookModel.setDescription(books_array.getJSONObject(i).getString("description"));
                promoBookModel.setPrice(books_array.getJSONObject(i).getString("price"));
                promoBookModel.setSearchKey(books_array.getJSONObject(i).getString("searchKey"));
                promoBookModel.setUrl(books_array.getJSONObject(i).getString("url"));
                promoBookModel.setTitle(books_array.getJSONObject(i).getString("title"));
                bookModels.add(promoBookModel);
                promoBookModel = null;
                Common.PROMO_BOOK_COUNT++;
            }
            System.out.println("----------" + Common.PROMO_BOOK_COUNT);
            Common.promoBookAdapter = new PromoBookAdapter(getApplicationContext(), bookModels);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Common.isConnected(getApplicationContext())) {

//                remoteConfig.setDefaultsAsync(R.xml.remote_config);
            remoteConfig.fetchAndActivate().addOnCompleteListener(new OnCompleteListener<Boolean>() {
                @Override
                public void onComplete(@NonNull Task<Boolean> task) {
                    if (task.isSuccessful()) {
                        System.out.println("------------REMOTE CONFIG UPDATED----------------------:" + task.getResult());
                        remoteConfig.activate();
                        setConfig();
                    }
                }
            });
        } else {

            workerThread();
        }

    }

}
