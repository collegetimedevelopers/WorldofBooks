package com.collegetime.theworldofbooks.EventBus;

public class HideFabEvent {
    boolean isSuccess;

    public HideFabEvent(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }
}
