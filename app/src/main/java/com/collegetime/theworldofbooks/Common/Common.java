package com.collegetime.theworldofbooks.Common;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;

import com.collegetime.theworldofbooks.Adapters.PromoBookAdapter;
import com.collegetime.theworldofbooks.Database.BookItem;
import com.collegetime.theworldofbooks.LoadBookDetail;
import com.collegetime.theworldofbooks.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.thin.downloadmanager.ThinDownloadManager;

import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Common {
    public static final String CANCEL_DOWNLOAD = "Cancel";
    public static final String PAUSE_DOWNLOAD = "Pause";
    public static final String RESUME_DOWNLOAD = "Resume";
    public static  boolean BOOK_EXPIRY_OFFER= false;
    public static boolean COUNT_BACK_PRESS = false;// to handle back press when user clicks back from search then before it was considering as a back press to solve this issue

    public static boolean HOT_LINK_ENABLED = false;
    public static String LAST_ONLINE;
    public static String BOOK_NAME;
    public static String BOOK_IMAGE = "";
    public static String BOOK_PATH;
    public static String EXPIRY_DATE;
    public static String SEARCH_DOMAIN = "www.libgen.is";
    public static String SEARCH_URL = "https://libgen.is/search.php?req=";
    public static String LOAD_BOOK_DETAILS_IP = "http://93.174.95.29/main/";
    public static String SEARCH_URL_ATTRIBUTES = "&open=0&res=100&view=simple&phrase=1&column=";
    public static int PROMO_BOOK_COUNT = 0;
    public static PromoBookAdapter promoBookAdapter;
    public static int CURRENT_FRAGMENT;
    public static boolean ShowPromo = false;
    public static boolean ShowCover = false;
    public static boolean AllowDownload = false;
    public static int SEARCH_COUNT = 1;
    public static String HOT_LINK = "https://od.lk/d/ODVfMTIwMDQ2NTBf/app-release.apk";

    public static HashMap<Integer, BookItem> downloadQueueMap = new HashMap<>();
    public static ThinDownloadManager thinDownloadManager = new ThinDownloadManager();
    public static List<BookItem> downloadedBooks;
//public static Context downloadContext;
//public  static DownloadManager downloadManager= (DownloadManager) downloadContext.getSystemService(Context.DOWNLOAD_SERVICE) ;


    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public static boolean isConnectedToServer() {
        try {
            URL myUrl = new URL(LOAD_BOOK_DETAILS_IP);
            URLConnection connection = myUrl.openConnection();
            connection.setConnectTimeout(2000);
            connection.connect();
            return true;
        } catch (Exception e) {
            // Handle your exceptions
            return false;
        }
    }


    public static boolean isExpired() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date;
        System.out.println("----------------------------EXPIRY DATE:" + Common.EXPIRY_DATE);
        Calendar cal = Calendar.getInstance();
        try {
            date = sdf.parse(Common.EXPIRY_DATE);
            if (System.currentTimeMillis() > date.getTime()) {
                // IT MEANS DOWNLOADS HAVE BEEN EXPIRED;
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isBookExpired(String expiryDate) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        try {

            date = sdf.parse(expiryDate);


            if (System.currentTimeMillis() > date.getTime()) {
                // IT MEANS DOWNLOADS HAVE BEEN EXPIRED;
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getLastOnline() throws ParseException {
        String lastOnline = " ";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date = sdf.parse(Common.LAST_ONLINE);
        lastOnline = sdf.format(date);
        //  String dateString = sdf.format(new Date(Long.parseLong(Common.LAST_ONLINE)));


        // lastOnline=date.toString();
        return lastOnline;
    }

    public static String convertToDate(String dateToBeConverted) throws ParseException {
        String lastOnline = " ";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date = sdf.parse(dateToBeConverted);
        lastOnline = sdf.format(date);
        //  String dateString = sdf.format(new Date(Long.parseLong(Common.LAST_ONLINE)));


        // lastOnline=date.toString();
        return lastOnline;
    }


}
