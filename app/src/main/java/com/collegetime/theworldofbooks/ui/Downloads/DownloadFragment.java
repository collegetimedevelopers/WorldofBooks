package com.collegetime.theworldofbooks.ui.Downloads;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.collegetime.theworldofbooks.Adapters.DownloadedBooksAdapter;
import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.Common.SwipeHelper;
import com.collegetime.theworldofbooks.Database.BookDataSource;
import com.collegetime.theworldofbooks.Database.BookDatabase;
import com.collegetime.theworldofbooks.Database.BookItem;
import com.collegetime.theworldofbooks.Database.LocalBookDataSource;
import com.collegetime.theworldofbooks.EventBus.DownloadCountEvent;
import com.collegetime.theworldofbooks.EventBus.HideFabEvent;
import com.collegetime.theworldofbooks.R;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DownloadFragment extends Fragment {

    @BindView(R.id.text_downlaods_info)
    TextView textDownlaodsInfo;
    @BindView(R.id.download_ebook)
    TextView txt_downloadEbook;
    @BindView(R.id.downloaded_book_recycler_view)
    RecyclerView downloaded_book_recycler_view;

    @BindView(R.id.empty_downloads)
    TextView empty_downloads;
    CompositeDisposable compositeDisposable;


    SharedPreferences preferences;
    DownloadedBooksAdapter downloadedBooksAdapter;
    private DownloadViewModel downloadViewModel;
    Unbinder unbinder;
    BookDataSource bookDataSource;
    EditText searchEditText;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        downloadViewModel =
                ViewModelProviders.of(this).get(DownloadViewModel.class);
        View root = inflater.inflate(R.layout.fragment_downloads, container, false);
        downloadViewModel.initBookDataSource(getContext());
        compositeDisposable = new CompositeDisposable();


        downloadViewModel.getDownloadedBooksliveData().observe(getViewLifecycleOwner(), new Observer<List<BookItem>>() {
            @Override
            public void onChanged(List<BookItem> bookItems) {

                if (null == bookItems || bookItems.isEmpty()) {
                    downloaded_book_recycler_view.setVisibility(View.GONE);
                    textDownlaodsInfo.setVisibility(View.GONE);
                    txt_downloadEbook.setVisibility(View.GONE);
                    empty_downloads.setVisibility(View.VISIBLE);


                } else {
                    Common.downloadedBooks = bookItems;// assigning books to list ( will be used for search )
                    downloaded_book_recycler_view.setVisibility(View.VISIBLE);
                    textDownlaodsInfo.setVisibility(View.VISIBLE);
                    txt_downloadEbook.setVisibility(View.VISIBLE);
                    empty_downloads.setVisibility(View.GONE);

                    downloadedBooksAdapter = new DownloadedBooksAdapter(getContext(), bookItems, getActivity());

                    if (appInstalledOrNot("com.github.collegetime.bookreader")) {

                        txt_downloadEbook.setVisibility(View.GONE);
                        textDownlaodsInfo.setVisibility(View.GONE);
                    }
                    downloaded_book_recycler_view.setAdapter(downloadedBooksAdapter);
                }
            }
        });

        unbinder = ButterKnife.bind(this, root);

        initViews();

        return root;
    }


    private void initViews() {
        txt_downloadEbook.setMovementMethod(LinkMovementMethod.getInstance());
        setHasOptionsMenu(true);
        bookDataSource = new LocalBookDataSource(BookDatabase.getInstance(getContext()).bookDAO());
        downloaded_book_recycler_view.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        downloaded_book_recycler_view.setLayoutManager(linearLayoutManager);
        downloaded_book_recycler_view.addItemDecoration(new DividerItemDecoration(getContext(), linearLayoutManager.getOrientation()));


        SwipeHelper swipeHelper = new SwipeHelper(getContext(), downloaded_book_recycler_view, 300) {
            @Override
            public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                underlayButtons.add(new SwipeHelper.UnderlayButton(getContext(), "DELETE", 35, 0, Color.parseColor("#FF0000"), new UnderlayButtonClickListener() {
                    @Override
                    public void onClick(int pos) {


                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setCancelable(false);
                        builder.setTitle("Delete Book");
                        builder.setMessage("Sure to Delete this book?");
                        builder.setPositiveButton("Yes, Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                BookItem bookItem = downloadedBooksAdapter.getItemAtPosition(pos);

                                bookDataSource.deleteBookItem(bookItem).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new SingleObserver<Integer>() {
                                            @Override
                                            public void onSubscribe(Disposable d) {

                                            }

                                            @Override
                                            public void onSuccess(Integer integer) {
                                                // DELETING BOOK FILE FROM STORAGE
                                                downloaded_book_recycler_view.setAdapter(downloadedBooksAdapter);

                                                try {

                                                    File bookFile = new File(bookItem.getBookPath());

                                                    if (bookFile.exists()) {
                                                        if (bookFile.delete()) {

                                                        }

                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();

                                                }

                                                // DELETING  IMAGE  FROM STORAGE
                                                try {
                                                    File coverImageFile = new File(bookItem.getBookImage());
                                                    if (coverImageFile.exists()) {
                                                        if (coverImageFile.delete()) {
                                                            Log.d("Image", "DELETED IMAGE//////////");
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                    Log.e("Image", "Failed to DELETED IMAGE//////////");
                                                }

                                                EventBus.getDefault().postSticky(new DownloadCountEvent(true));
                                                Toast.makeText(getContext(), "Book Deleted  ", Toast.LENGTH_LONG).show();

                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                                Toast.makeText(getContext(), "Failed to delete book, Please try again.", Toast.LENGTH_LONG).show();
                                            }
                                        });
                            }
                        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Toast.makeText(getContext(), "You saved your book \uD83D\uDE1C ", Toast.LENGTH_SHORT).show();

                            }
                        });

                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }));
            }
        };

    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = requireContext().getPackageManager();

        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);

            return true;
        } catch (PackageManager.NameNotFoundException e) {

        }
        return false;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().postSticky(new HideFabEvent(true));

        Common.CURRENT_FRAGMENT = R.id.nav_download;

        try {
            FirebaseAnalytics.getInstance(getContext()).logEvent("DOWNLOAD_FRAGMENT_OPENED", null);
            FirebaseInAppMessaging.getInstance().triggerEvent("DOWNLOAD_FRAGMENT_OPENED");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void showExpiredDialog() throws ParseException {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.WarningMessageAlertDialogStyle);
        builder.setCancelable(false);
        builder.setMessage("Your Downloads have been expired because you were last online on " + Common.getLastOnline() + " on this app, you need to come online at least once in a week. Please bring the app online to refresh the downloads and continue reading again.");
        builder.setTitle("ATTENTION");

        builder.setPositiveButton("GOT IT ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                preferences = getContext().getSharedPreferences("AppData", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("showAds", "true");
                editor.apply();

                requireActivity().moveTaskToBack(true);
                requireActivity().finishAffinity();

            }
        });


        AlertDialog messageDialog = builder.create();
        messageDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                messageDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(android.R.color.holo_green_light));
            }
        });
        messageDialog.show();
    }


    //
//    @Override
//    public void onStop() {
//        downloadViewModel.onstop();
//        compositeDisposable.clear();
//              super.onStop();
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        downloadViewModel.initBookDataSource(getContext());
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        compositeDisposable.clear();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        downloadViewModel.initBookDataSource(getContext());
//
//    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_bar, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) getContext().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setQueryHint("Search By Book Name");

        searchEditText = (EditText) searchView.findViewById(R.id.search_src_text);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                startSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                startSearch(newText);

                return true;
            }
        });

        ImageView closeButton = searchView.findViewById(R.id.search_close_btn);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setText("");
                searchView.setQueryHint("Search By Book Name");
                searchView.setQuery("", false);
                searchView.onActionViewCollapsed();
                menuItem.collapseActionView();

                try {
                    if (null != Common.downloadedBooks && !Common.downloadedBooks.isEmpty()) {
                        downloadViewModel.getDownloadedBooksliveData().setValue(Common.downloadedBooks);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

    }

    private void startSearch(String query) {
        List<BookItem> searchResult = new ArrayList<>();

        if (null != Common.downloadedBooks && !Common.downloadedBooks.isEmpty()) {
            for (BookItem book : Common.downloadedBooks) {
                if (book.getBookName().trim().toLowerCase().contains(query.trim().toLowerCase())) {

                    searchResult.add(book);
                }

                downloadViewModel.setDownloadedBooksliveData(searchResult);
            }
        }

    }

}

