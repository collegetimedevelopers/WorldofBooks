package com.collegetime.theworldofbooks.Database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface BookDAO {

    @Query("Select * FROM Books")
    Flowable<List<BookItem>> getAllBooks();

    @Query("Select COUNT(*) from Books")
    Single<Integer> getDownloadedBooksCount();

//    @Query("Select * from books where bookName like bookname ")
//    Flowable<List<BookItem>> searchByBookName(String bookname);
//

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable InsertBook(BookItem... bookItems);

    @Delete
    Single<Integer> deleteBookItem(BookItem bookItems);

    @Query("Delete FROM Books WHERE bookId =:id")
    Single<Integer> deleteABook(Integer id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    Completable UpdateBook(BookItem... bookItems);

    @Query("SELECT * FROM Books WHERE favourateBook =1")
    Flowable<List<BookItem>> getFavouriteBooks();

    @Query("SELECT * FROM Books WHERE currentlyReadingBook =1")
    Flowable<List<BookItem>> getcurrentlyReadingBook();

    @Query("SELECT * FROM Books WHERE bookId like :searchedBookId")
    Flowable<List<BookItem>>  searchBook(String searchedBookId);
}
