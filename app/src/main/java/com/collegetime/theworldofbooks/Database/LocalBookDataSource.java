package com.collegetime.theworldofbooks.Database;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class LocalBookDataSource implements BookDataSource{
    BookDAO bookDAO;

    public LocalBookDataSource(BookDAO bookDAO) {
        this.bookDAO = bookDAO;
    }

    @Override
    public Flowable<List<BookItem>> getAllBooks() {
        return bookDAO.getAllBooks() ;
    }

    @Override
    public Single<Integer> getDownloadedBooksCount() {
        return bookDAO.getDownloadedBooksCount();
    }

    @Override
    public Completable InsertBook(BookItem... bookItems) {
        return bookDAO.InsertBook(bookItems);
    }

    @Override
    public Single<Integer> deleteBookItem(BookItem bookItems) {
        return bookDAO.deleteBookItem(bookItems);
    }

    @Override
    public Single<Integer> deleteABook(Integer id) {
        return bookDAO.deleteABook(id);
    }

    @Override
    public Completable UpdateBook(BookItem... bookItems) {
        return bookDAO.UpdateBook(bookItems);
    }

    @Override
    public Flowable<List<BookItem>> getFavouriteBooks() {
        return bookDAO.getFavouriteBooks();
    }

    @Override
    public Flowable<List<BookItem>> getcurrentlyReadingBook() {
        return bookDAO.getcurrentlyReadingBook();
    }

    @Override
    public  Flowable<List<BookItem>> searchBook(String searchedBookId) {
        return bookDAO.searchBook(searchedBookId);
    }
}
