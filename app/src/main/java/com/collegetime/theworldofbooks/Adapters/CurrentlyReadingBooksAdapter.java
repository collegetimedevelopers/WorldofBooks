package com.collegetime.theworldofbooks.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.Database.BookDataSource;
import com.collegetime.theworldofbooks.Database.BookDatabase;
import com.collegetime.theworldofbooks.Database.BookItem;
import com.collegetime.theworldofbooks.Database.LocalBookDataSource;
import com.collegetime.theworldofbooks.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CurrentlyReadingBooksAdapter extends RecyclerView.Adapter<CurrentlyReadingBooksAdapter.MyViewHolder> {

    Context context;
    List<BookItem> currentlyReadingBookItemList;
    CompositeDisposable compositeDisposable;
    BookDataSource bookDataSource;
    RewardedAd rewardedAd;
    Activity activity;
    ProgressDialog progressDialog;

    public CurrentlyReadingBooksAdapter(Context context, List<BookItem> currentlyReadingBookItemList, Activity activity) {
        this.context = context;
        this.currentlyReadingBookItemList = currentlyReadingBookItemList;
        this.compositeDisposable = new CompositeDisposable();
        this.bookDataSource = new LocalBookDataSource(BookDatabase.getInstance(context).bookDAO());
        activity = this.activity;

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");

        List<String> testDeviceIds = Arrays.asList("1D534D8BB4D7074CC5266E4811EEBD5E");
        RequestConfiguration configuration =
                new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
        MobileAds.setRequestConfiguration(configuration);

        MobileAds.initialize(context, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

            }
        });


    }

    @NonNull
    @Override
    public CurrentlyReadingBooksAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_currently_reading, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CurrentlyReadingBooksAdapter.MyViewHolder holder, int position) {

        try {
            Bitmap bitmap = BitmapFactory.decodeFile(currentlyReadingBookItemList.get(position).getBookImage());
            holder.downloaded_book_image.setImageBitmap(bitmap);


        } catch (Exception e) {
            e.printStackTrace();
        }


        if (currentlyReadingBookItemList.get(position).isFavourateBook()) {
            int color = Color.argb(255, 255, 0, 0);
            holder.img_favourate_book.setColorFilter(color);
        }

        if (Common.isBookExpired(currentlyReadingBookItemList.get(position).getDownload_expiry_date())) {
            holder.frame_expired_layout.setVisibility(View.VISIBLE);
        } else {
            holder.frame_expired_layout.setVisibility(View.GONE);

        }

        holder.txt_downloaded_book_title.setText(currentlyReadingBookItemList.get(position).getBookName());

        try {

            File bookFile = new File(currentlyReadingBookItemList.get(position).getBookPath());

            if (!bookFile.exists()) {
                //holder.layout.setBackgroundColor(Color.RED);
                holder.downloaded_book_error.setVisibility(View.VISIBLE);
                //   holder.img_currently_reading.setVisibility(View.GONE);
                holder.img_favourate_book.setVisibility(View.GONE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.img_favourate_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!currentlyReadingBookItemList.get(position).isFavourateBook()) {
                    BookItem bookItem = new BookItem();
                    bookItem = currentlyReadingBookItemList.get(position);
                    bookItem.setFavourateBook(true);
                    compositeDisposable.add(bookDataSource.UpdateBook(bookItem)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(() -> {

                                currentlyReadingBookItemList.get(position).setFavourateBook(true);

                                int color = Color.argb(255, 255, 0, 0);
                                holder.img_favourate_book.setColorFilter(color);


                            }, throwable -> {

                                Toast.makeText(context, "Failed to add to Currently Reading Books", Toast.LENGTH_SHORT).show();

                            }));
                } else {
                    BookItem bookItem = new BookItem();
                    bookItem = currentlyReadingBookItemList.get(position);
                    bookItem.setFavourateBook(false);
                    compositeDisposable.add(bookDataSource.UpdateBook(bookItem).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {

                        currentlyReadingBookItemList.get(position).setFavourateBook(false);

                        int color = Color.argb(255, 167, 167, 167);
                        holder.img_favourate_book.setColorFilter(color);

                    }, throwable -> {

                        Toast.makeText(context, "Failed to remove from Currently Reading Books", Toast.LENGTH_SHORT).show();

                    }));
                }
            }
        });

        holder.img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    shareBook(position, holder);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Can,t Share, Some Error Occurred", Toast.LENGTH_SHORT).show();

                }
                Animation animation;
                animation = AnimationUtils.loadAnimation(context, R.anim.blink_animation);
                holder.img_share.startAnimation(animation);
            }
        });

    }

    //  S H A R E       B O O K
    private void shareBook(int position, MyViewHolder holder) throws Exception {

        File sharedFile = new File(currentlyReadingBookItemList.get(position).getBookPath());


        if (sharedFile.exists()) {
            Uri imageUri = getImageUri(holder);
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, imageUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("image/png");
            SpannableStringBuilder str = new SpannableStringBuilder(currentlyReadingBookItemList.get(position).getBookName());
            str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 0, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            String fileSize = "unk";
            if (sharedFile.length() / 1024 < 1024) {
                // if the file  size is in KB
                fileSize = sharedFile.length() / 1024 + "Kb";


            } else {
                // if the file  size is in MB
                fileSize = (sharedFile.length() / 1024) / 1024 + "Mb";

            }

            DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse(currentlyReadingBookItemList.get(position).getBookId() + "ext"
                            + currentlyReadingBookItemList.get(position).getBookExt() + "lng" + "unknown" + "dwn" +
                            String.valueOf(fileSize)))
                    .setDomainUriPrefix("https://theworldofbooks.page.link/book/")
                    .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                    .buildDynamicLink();
            Uri dynamicLinkUri = dynamicLink.getUri();
            System.out.println("------------------------------" + dynamicLinkUri + "-------------------------------");
            String msg = "I am reading *" + str + "* on *World of Books*. \nOpen it Now : \n" + dynamicLinkUri + "\n";
            intent.putExtra(Intent.EXTRA_TEXT, msg);

            try {
                FirebaseAnalytics.getInstance(context).logEvent("BOOK_SHARED_FROM_DOWNLOADS", null);
                FirebaseInAppMessaging.getInstance().triggerEvent("BOOK_SHARED_FROM_DOWNLOADS");
            } catch (Exception e) {
                e.printStackTrace();
            }
            context.startActivity(intent);
        } else {
            // means file is not present so we can share but size can't be detected
            Uri imageUri = getImageUri(holder);
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, imageUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setType("image/png");
            SpannableStringBuilder str = new SpannableStringBuilder(currentlyReadingBookItemList.get(position).getBookName());
            str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 0, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse(currentlyReadingBookItemList.get(position).getBookId() + "ext" + currentlyReadingBookItemList.get(position).getBookExt() + "lng" + "unknown" + "dwn" + "unk"))
                    .setDomainUriPrefix("https://theworldofbooks.page.link/book/")
                    .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                    .buildDynamicLink();
            Uri dynamicLinkUri = dynamicLink.getUri();
            System.out.println("------------------------------" + dynamicLinkUri + "-------------------------------");
            String msg = "I am reading *" + str + "* on *World of Books*. \nOpen it Now : \n" + dynamicLinkUri + "\n";
            intent.putExtra(Intent.EXTRA_TEXT, msg);

            try {
                FirebaseAnalytics.getInstance(context).logEvent("BOOK_SHARED_FROM_DOWNLOADS", null);
                FirebaseInAppMessaging.getInstance().triggerEvent("BOOK_SHARED_FROM_DOWNLOADS");
            } catch (Exception e) {
                e.printStackTrace();
            }
            context.startActivity(intent);


        }
    }

    private Uri getImageUri(MyViewHolder holder) throws Exception {
        holder.downloaded_book_image.setDrawingCacheEnabled(true);
        holder.downloaded_book_image.buildDrawingCache();
        Bitmap image = null;
        try {
            image = ((BitmapDrawable) holder.downloaded_book_image.getDrawable()).getBitmap();
        } catch (Exception e) {
            e.printStackTrace();
        }

        File imagesFolder = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Uri uri = null;
        try {
            imagesFolder.mkdirs();
            File file = new File(imagesFolder, "shared_image.png");

            FileOutputStream stream = new FileOutputStream(file);
            assert image != null;
            image.compress(Bitmap.CompressFormat.PNG, 50, stream);
            stream.flush();
            stream.close();
            uri = FileProvider.getUriForFile(context, "com.collegetime.theworldofbooks.fileprovider", file);

        } catch (IOException e) {
            Log.d("SHR_IMG", "IOException while trying to write file for sharing: " + e.getMessage());
        }
        return uri;
    }


    // R E W A R D      A N D       V A L I D I T Y     E X T E N S I O N
    private void loadRewardedVideo(int position) {
        progressDialog.show();

        if (rewardedAd == null || !rewardedAd.isLoaded()) {
            rewardedAd = new RewardedAd(context, context.getString(R.string.rewarded_video_book_validity_ext));

            RewardedAdLoadCallback rewardedAdLoadCallback = new RewardedAdLoadCallback() {
                @Override
                public void onRewardedAdLoaded() {
                    super.onRewardedAdLoaded();
                    showRewardedVideo(position);

                }

                @Override
                public void onRewardedAdFailedToLoad(int i) {
                    super.onRewardedAdFailedToLoad(i);
                    progressDialog.dismiss();
                    Toast.makeText(context, "Ad Failed to Load,Please try after some time", Toast.LENGTH_LONG).show();

                }
            };
            rewardedAd.loadAd(new AdRequest.Builder().build(), rewardedAdLoadCallback);
        }


    }


    private void showRewardedVideo(int position) {
        if (null != rewardedAd && rewardedAd.isLoaded()) {
            RewardedAdCallback rewardedAdCallback = new RewardedAdCallback() {
                @Override
                public void onRewardedAdOpened() {
                    super.onRewardedAdOpened();

                }

                @Override
                public void onRewardedAdClosed() {
                    super.onRewardedAdClosed();

                }

                @Override
                public void onRewardedAdFailedToShow(int i) {
                    super.onRewardedAdFailedToShow(i);
                    Toast.makeText(context, "Ad Failed to Show, Please Try Again.", Toast.LENGTH_LONG).show();

                }

                @Override
                public void onUserEarnedReward(@NonNull com.google.android.gms.ads.rewarded.RewardItem rewardItem) {

                    try {
                        handleReward(position, rewardItem);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

            };
            progressDialog.dismiss();
            rewardedAd.show(activity, rewardedAdCallback);

        } else {
            loadRewardedVideo(position);
        }
    }

    private void handleReward(int position, RewardItem rewardItem) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String validTill = new String();
        Date oldExpiryDate, newExpiryDate;
        switch (rewardItem.getAmount()) {

            case 1:
                // this case will be used in normal days when we are giving validity of 15 days
                calendar = Calendar.getInstance();
                oldExpiryDate = dateFormat.parse(currentlyReadingBookItemList.get(position).getDownload_expiry_date());
                calendar.setTime(oldExpiryDate);
                calendar.add(Calendar.DATE, 15);
                newExpiryDate = calendar.getTime();
                validTill = dateFormat.format(newExpiryDate);
                System.out.println("OLD EXPIRY DATE  (case 1 ) ====== " + Common.convertToDate(dateFormat.format(oldExpiryDate)));
                System.out.println("NEW EXPIRY DATE  (case 1 ) ====== " + Common.convertToDate(validTill));

                extendBookValidity(position, validTill);
                break;

            case 2:
                // this case will be used in offer
                // when we are giving double validity (like on eve of  Saraswati puja etc.)
                calendar = Calendar.getInstance();
                oldExpiryDate = dateFormat.parse(currentlyReadingBookItemList.get(position).getDownload_expiry_date());
                calendar.setTime(oldExpiryDate);
                calendar.add(Calendar.MONTH, 1);
                newExpiryDate = calendar.getTime();
                validTill = dateFormat.format(newExpiryDate);
                System.out.println("OLD EXPIRY DATE (case 2 ) ====== " + Common.convertToDate(dateFormat.format(oldExpiryDate)));
                System.out.println("NEW EXPIRY DATE (case 2 )====== " + Common.convertToDate(validTill));

                extendBookValidity(position, validTill);

                break;
        }
    }

    private void extendBookValidity(int position, String validTill) {
        BookItem bookItem = getItemAtPosition(position);
        bookItem.setDownload_expiry_date(validTill);

        compositeDisposable.add(bookDataSource.UpdateBook(bookItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {

                    currentlyReadingBookItemList.get(position).setDownload_expiry_date(validTill);
                    progressDialog.dismiss();
                    notifyDataSetChanged();
                    Toast.makeText(context, "Book Validity Extended", Toast.LENGTH_LONG).show();

                }, throwable -> {
                    Toast.makeText(context, "Failed to Extend Book Validity ", Toast.LENGTH_LONG).show();

                }));
    }


    @Override
    public int getItemCount() {
        return currentlyReadingBookItemList.size();
    }

    public BookItem getItemAtPosition(int pos) {
        return currentlyReadingBookItemList.get(pos);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements ViewGroup.OnClickListener {

        Unbinder unbinder;

        @BindView(R.id.downloaded_book_image)
        ImageView downloaded_book_image;

        @BindView(R.id.txt_downloaded_book_title)
        TextView txt_downloaded_book_title;

        @BindView(R.id.downloaded_layout)
        LinearLayout layout;

        @BindView(R.id.download_book_error)
        ImageView downloaded_book_error;

        @BindView(R.id.add_to_favourate_book)
        ImageView img_favourate_book;


        @BindView(R.id.img_share)
        ImageView img_share;


        // expiry ui
        @BindView(R.id.frame_expired_layout)
        FrameLayout frame_expired_layout;


        @BindView(R.id.txt_downloaded_book_refresh)
        TextView txt_refresh_book;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (!Common.isBookExpired(currentlyReadingBookItemList.get(position).getDownload_expiry_date())) {
                try {
                    File bookFile = new File(currentlyReadingBookItemList.get(position).getBookPath());

                    if (bookFile.exists()) {
                        MimeTypeMap myMime = MimeTypeMap.getSingleton();
                        Intent newIntent = new Intent(Intent.ACTION_VIEW);
                        String mimeType = "application/" + currentlyReadingBookItemList.get(position).getBookExt();
                        File open = new File(currentlyReadingBookItemList.get(position).getBookPath());
                        String packageName = context.getApplicationContext().getPackageName();

                        Uri uri = FileProvider.getUriForFile(context, "com.collegetime.theworldofbooks.fileprovider", open);
                        context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        newIntent.setType(mimeType);
                        System.out.println("--------------------------------MIME " + mimeType + "--------------------------");
                        newIntent.setData(uri);
                        //            newIntent.putExtra(Intent.EXTRA_STREAM,uri);
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        newIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        try {
                            context.startActivity(newIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(context, "No handler for this type of file.", Toast.LENGTH_LONG).show();
                        }
                        try {
                            FirebaseAnalytics.getInstance(context).logEvent("BOOK_OPENED_FROM_CURRENTLY_READING_BOOKS", null);
                            FirebaseInAppMessaging.getInstance().triggerEvent("BOOK_OPENED_FROM_CURRENTLY_READING_BOOKS");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setCancelable(false);
                        builder.setTitle("File Not Found");
                        builder.setMessage("The requested file has been moved or is missing. Please Download it again.");
                        builder.setPositiveButton("I Understand", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Some error occurred while opening the file", Toast.LENGTH_SHORT).show();
                }
            }
            else {
            // book is expired
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.WarningMessageAlertDialogStyle);
            builder.setCancelable(false);
            builder.setMessage("The requested file has been expired. You can extend its validity by watching a Ad. We know that ads are painful but without ad revenue, we wouldn't even be here. And we might not be here much longer because " +
                    "these are the only way to maintain our servers .");
            builder.setPositiveButton("EXTEND", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    loadRewardedVideo(position);
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
        }
    }
}





