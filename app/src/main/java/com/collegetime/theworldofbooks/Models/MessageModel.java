package com.collegetime.theworldofbooks.Models;

public class MessageModel {
    private String message;
    private boolean recurring;

    public MessageModel(String message,boolean recurring) {
        this.message = message;

        this.recurring = recurring;
    }
    public MessageModel()
    {

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }



    public boolean isRecurring() {
        return recurring;
    }

    public void setRecurring(boolean recurring) {
        this.recurring = recurring;
    }
}
