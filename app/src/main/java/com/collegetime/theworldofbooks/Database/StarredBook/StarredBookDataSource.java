package com.collegetime.theworldofbooks.Database.StarredBook;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface StarredBookDataSource {

    Flowable<List<StarredBookItems>> getAllStarredBooks();


    Completable insertStarredBook(StarredBookItems...starredBookItems);


    Single<Integer> deleteStarredBook(StarredBookItems starredBookItems);

}
