package com.collegetime.theworldofbooks.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.collegetime.theworldofbooks.Executors.Search;
import com.collegetime.theworldofbooks.Models.LinksModel;
import com.collegetime.theworldofbooks.Models.SearchResult;

import java.util.ArrayList;

public class HomeViewModel extends ViewModel {

    public static MutableLiveData<ArrayList<SearchResult>> resultsList;
    public static MutableLiveData<ArrayList<LinksModel>> links ;

    public HomeViewModel() {
       resultsList = new MutableLiveData<>();
       links = new MutableLiveData<>();

    }

    public LiveData<ArrayList<SearchResult>> getText() {
        return resultsList;
    }
    public LiveData<ArrayList<LinksModel>>getLinks(){return  links;}
}