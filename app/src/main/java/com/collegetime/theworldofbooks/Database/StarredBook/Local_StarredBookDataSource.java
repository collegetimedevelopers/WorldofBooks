package com.collegetime.theworldofbooks.Database.StarredBook;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class Local_StarredBookDataSource  implements StarredBookDao{

    @Override
    public Flowable<List<StarredBookItems>> getAllStarredBooks() {
        return getAllStarredBooks();
    }

    @Override
    public Completable insertStarredBook(StarredBookItems... starredBookItems) {
        return insertStarredBook(starredBookItems);
    }

    @Override
    public Single<Integer> deleteStarredBook(StarredBookItems starredBookItems) {
        return deleteStarredBook(starredBookItems);
    }
}
