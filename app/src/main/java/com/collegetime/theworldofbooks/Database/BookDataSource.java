package com.collegetime.theworldofbooks.Database;

import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface BookDataSource  {

    Flowable<List<BookItem>> getAllBooks();

     Single<Integer> getDownloadedBooksCount();


     Completable InsertBook(BookItem...bookItems);

//    Completable InsertBook(String bookName,String bookImagePath,String bookPath);

    Single<Integer> deleteBookItem(BookItem bookItems);

     Single<Integer> deleteABook(Integer id);

    Completable UpdateBook(BookItem...bookItems);

     Flowable<List<BookItem>> getFavouriteBooks();

     Flowable<List<BookItem>> getcurrentlyReadingBook();

    Flowable<List<BookItem>> searchBook(String searchedBookId);

}
