package com.collegetime.theworldofbooks.ui.Downloads;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.collegetime.theworldofbooks.Database.BookDataSource;
import com.collegetime.theworldofbooks.Database.BookDatabase;
import com.collegetime.theworldofbooks.Database.BookItem;
import com.collegetime.theworldofbooks.Database.LocalBookDataSource;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class DownloadViewModel extends ViewModel {

    private MutableLiveData<List<BookItem>> downloadedBooksliveData;
    BookDataSource bookDataSource;
    CompositeDisposable  compositeDisposable;

    public DownloadViewModel() {
      compositeDisposable= new CompositeDisposable();
    }

    public void initBookDataSource(Context context){
        bookDataSource= new LocalBookDataSource(BookDatabase.getInstance(context).bookDAO());
    }

    public MutableLiveData<List<BookItem>> getDownloadedBooksliveData() {
        if (downloadedBooksliveData==null)
            downloadedBooksliveData= new MutableLiveData<>();
        getAllDownloadedBooks();

        return downloadedBooksliveData;
    }

    private void getAllDownloadedBooks() {
        compositeDisposable.add(bookDataSource.getAllBooks().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<BookItem>>() {
            @Override
            public void accept(List<BookItem> bookItems) throws Exception {
                downloadedBooksliveData.setValue(bookItems);
            }
        },throwable -> {
            downloadedBooksliveData.setValue(null);
        }));
    }


    public void setDownloadedBooksliveData(List<BookItem> downloadedBookslist) {
      downloadedBooksliveData.setValue(downloadedBookslist); ;
    }



    public void onStop()
    {
        compositeDisposable.clear();
    }
}