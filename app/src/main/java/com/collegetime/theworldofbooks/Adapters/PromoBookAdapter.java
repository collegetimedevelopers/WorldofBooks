package com.collegetime.theworldofbooks.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.collegetime.theworldofbooks.Executors.Search;
import com.collegetime.theworldofbooks.Models.PromoBookModel;
import com.collegetime.theworldofbooks.R;
import com.collegetime.theworldofbooks.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.Collections;


public class PromoBookAdapter extends RecyclerView.Adapter<PromoBookAdapter.ResultViewHolder> {
    ArrayList<PromoBookModel>promo_list;
    Context context;

    public PromoBookAdapter(Context context,ArrayList<PromoBookModel> promo_list) {
        Collections.shuffle(promo_list);
        this.promo_list = promo_list;
        this.context = context;

    }

    @NonNull
    @Override
    public PromoBookAdapter.ResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.layout_promrotion_book,parent,false);
        return new ResultViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PromoBookAdapter.ResultViewHolder holder, int position) {
        holder.title.setText(promo_list.get(position).getTitle());
        holder.price.setText("Worth \u20b9"+promo_list.get(position).getPrice());
        holder.description.setText(promo_list.get(position).getDescription());
        Glide.with(context).load(promo_list.get(position).getUrl()).into(holder.book_cover);

    }

    @Override
    public int getItemCount() {
        return promo_list.size();
    }

    public class ResultViewHolder extends RecyclerView.ViewHolder implements ViewGroup.OnClickListener {
        TextView title, price,description;
        ImageView book_cover;
        public ResultViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.promo_book_title);
            price = itemView.findViewById(R.id.promo_price);
            description = itemView.findViewById(R.id.promo_description);
            book_cover = itemView.findViewById(R.id.promo_book_image);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            String key = promo_list.get(position).getSearchKey();
            HomeFragment.promoBookSearch(key);


        }
    }
    public void shuffleBooks()
    {
        Collections.shuffle(promo_list);

    }
}
