package com.collegetime.theworldofbooks.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.Executors.PrepareSearchResult;
import com.collegetime.theworldofbooks.LoadBookDetail;
import com.collegetime.theworldofbooks.Models.SearchResult;
import com.collegetime.theworldofbooks.R;
import com.collegetime.theworldofbooks.ui.home.HomeFragment;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;


public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ResultViewHolder> {
    Context context;
    private InterstitialAd interstitialAd;
    int resource;
    ArrayList<SearchResult> list;
    public SearchResultAdapter(Context context, ArrayList<SearchResult> list)
    {
        this.context = context;
        this.list = list;

        interstitialAd = new InterstitialAd(context);
        interstitialAd.setAdUnitId("ca-app-pub-7936495263143595/2976656820");

        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener()
        {
            @Override
            public void onAdClosed()
            {
                interstitialAd.loadAd(new AdRequest.Builder().build());
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                interstitialAd.loadAd(new AdRequest.Builder().build());

            }

        });
    }
    @NonNull
    @Override
    public ResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.card_view_book,parent,false);
        return new ResultViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ResultViewHolder holder, int position) {
        String title = list.get(position).getBook_tile();
        String publisher = list.get(position).getPublisher();
        holder.publisher.setText(publisher);
        holder.title.setText(title);


    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ResultViewHolder extends  RecyclerView.ViewHolder implements ViewGroup.OnClickListener
    {
        TextView title, publisher;
        public ResultViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            publisher = itemView.findViewById(R.id.publisher);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            Intent i =new Intent(context, LoadBookDetail.class);
            i.putExtra("link", HomeFragment.links.get(position).getLink());
            i.putExtra("language",HomeFragment.resultArrayList.get(position).getLanguage());
            i.putExtra("extension",HomeFragment.resultArrayList.get(position).getExtension());
            i.putExtra("size",HomeFragment.resultArrayList.get(position).getSize());
            System.out.println("-------------------------------SEARCH COUNT :"+Common.SEARCH_COUNT);
            if(Common.SEARCH_COUNT %4 ==0) {
                if(position %2 ==0) {
                    if (interstitialAd.isLoaded()) {
                        context.startActivity(i);
                        interstitialAd.show();

                    } else {
                        context.startActivity(i);
                    }
                }else{
                    context.startActivity(i);
                }
            }else{
                context.startActivity(i);
            }

        }
    }
}
