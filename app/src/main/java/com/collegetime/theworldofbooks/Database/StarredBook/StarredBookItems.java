package com.collegetime.theworldofbooks.Database.StarredBook;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.sql.Struct;

@Entity(tableName = "StarredBooks",indices = {@Index(value = "BookName",unique = true)})
public class StarredBookItems {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "BookId")
    int BookId;

    @ColumnInfo(name = "BookName")
    String BookName;

    @ColumnInfo(name = "Publisher",defaultValue = "Not Mentioned")
    String Publisher;

    @ColumnInfo(name = "Author",defaultValue = "Not Mentioned")
    String Author;

    @ColumnInfo(name = "BookSize",defaultValue = "Not Mentioned")
    String BookSize;

    @ColumnInfo(name = "Language",defaultValue = "Not Mentioned")
    String Language;

    @ColumnInfo(name = "Book_CostInCredits",defaultValue = "For Free")
    String Book_CostInCredits;

    @ColumnInfo(name = "Year",defaultValue = "Not Mentioned")
    String Year;

    @ColumnInfo(name = "Cover_image_link")
    String Cover_image_link;

    @ColumnInfo(name = "Download_link")
    String Download_link;

    @ColumnInfo(name = "ISBN",defaultValue = "Not Mentioned")
    String Isbn;

    @ColumnInfo(name = "Description",defaultValue = "Description Not Available")
    String Description;

    public StarredBookItems(@NonNull int bookId, String bookName, String publisher, String author, String bookSize, String language, String book_CostInCredits, String year, String cover_image_link, String download_link, String isbn, String description) {
        BookId = bookId;
        BookName = bookName;
        Publisher = publisher;
        Author = author;
        BookSize = bookSize;
        Language = language;
        Book_CostInCredits = book_CostInCredits;
        Year = year;
        Cover_image_link = cover_image_link;
        Download_link = download_link;
        Isbn = isbn;
        Description = description;
    }

    public StarredBookItems() {
    }

    @NonNull
    public int getBookId() {
        return BookId;
    }

    public void setBookId(@NonNull int bookId) {
        BookId = bookId;
    }

    public String getBookName() {
        return BookName;
    }

    public void setBookName(String bookName) {
        BookName = bookName;
    }

    public String getPublisher() {
        return Publisher;
    }

    public void setPublisher(String publisher) {
        Publisher = publisher;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getBookSize() {
        return BookSize;
    }

    public void setBookSize(String bookSize) {
        BookSize = bookSize;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getBook_CostInCredits() {
        return Book_CostInCredits;
    }

    public void setBook_CostInCredits(String book_CostInCredits) {
        Book_CostInCredits = book_CostInCredits;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public String getCover_image_link() {
        return Cover_image_link;
    }

    public void setCover_image_link(String cover_image_link) {
        Cover_image_link = cover_image_link;
    }

    public String getDownload_link() {
        return Download_link;
    }

    public void setDownload_link(String download_link) {
        Download_link = download_link;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getIsbn() {
        return Isbn;
    }

    public void setIsbn(String isbn) {
        Isbn = isbn;
    }
}
