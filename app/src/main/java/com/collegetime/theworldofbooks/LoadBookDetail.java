package com.collegetime.theworldofbooks;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.room.ColumnInfo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.collegetime.theworldofbooks.BroadcastReciever.NotificationReceiver;
import com.collegetime.theworldofbooks.Common.Common;
import com.collegetime.theworldofbooks.Database.BookDataSource;
import com.collegetime.theworldofbooks.Database.BookDatabase;
import com.collegetime.theworldofbooks.Database.BookItem;
import com.collegetime.theworldofbooks.Database.LocalBookDataSource;
import com.collegetime.theworldofbooks.EventBus.DownloadCountEvent;
import com.collegetime.theworldofbooks.EventBus.HideFabEvent;
import com.collegetime.theworldofbooks.EventBus.showTitleEvent;
import com.collegetime.theworldofbooks.Executors.LoadBook;
import com.collegetime.theworldofbooks.Services.DownloadService;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.downloader.Progress;
import com.downloader.httpclient.HttpClient;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.ThrowOnExtraProperties;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.OkHttp3Downloader;
import com.thin.downloadmanager.DefaultRetryPolicy;
import com.thin.downloadmanager.DownloadRequest;
import com.thin.downloadmanager.DownloadStatusListener;
import com.thin.downloadmanager.DownloadStatusListenerV1;
import com.thin.downloadmanager.RetryError;
import com.thin.downloadmanager.RetryPolicy;
import com.thin.downloadmanager.ThinDownloadManager;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchConfiguration;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.NetworkType;
import com.tonyodev.fetch2.Priority;
import com.tonyodev.fetch2.Request;
import com.tonyodev.fetch2core.DownloadBlock;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Connection;
import okhttp3.OkHttpClient;

import static android.net.wifi.WifiConfiguration.Status.strings;

public class LoadBookDetail extends AppCompatActivity implements FetchListener {

    private static final int REQUEST_PERMISSION_SETTING = 7070;
    public static ImageView cover;
    public static TextView title;
    public static TextView year;
    public static TextView author;
    public static TextView publisher;
    public static TextView isbn;

    boolean download_button_clicked = false;
    boolean enableDownloadButton;

    public static CoordinatorLayout layout;
    public static String downloadLink;
    public static TextView description;
    AdView adView;
    TextView language, topic;
    LinearLayout share;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    FrameLayout ad_view_container;
    Button download;
    String extension, link;
    String size_text = null;
    String lang = null;
    String[] stringSplit = {"", ""};
    AppBarLayout appBarLayout;
    CardView book_detail_card_view_available, book_detail_card_view_language;

    // in testing'
    NotificationChannel channel;

    NotificationManagerCompat mNotification;
    NotificationCompat.Builder builder;

    CompositeDisposable compositeDisposable;
    BookDataSource bookDataSource;
    BookDatabase bookDatabase;
    boolean isBookAlreadyDownloaded;

    // test 2
    PRDownloader prDownloader;

    // test 3
    Fetch fetch;
    FetchListener fetchListener;
    int i = 0;

    // test 4
    long lastDownload;
    DownloadManager downloadManager;
    DownloadManager.Request downloadRequest;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_book_detail);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        layout = findViewById(R.id.book_view);
        layout.setVisibility(View.INVISIBLE);


        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setReadTimeout(25000)
                .setConnectTimeout(25000)
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(getApplicationContext(), config);


        FetchConfiguration fetchConfiguration = new FetchConfiguration.Builder(this)
                .setDownloadConcurrentLimit(3)

                .build();

        fetch = Fetch.Impl.getInstance(fetchConfiguration);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);

        //SETTING UP DATABASE STUFFS
        bookDatabase = BookDatabase.getInstance(LoadBookDetail.this);
        bookDataSource = new LocalBookDataSource(BookDatabase.getInstance(LoadBookDetail.this).bookDAO());
        compositeDisposable = new CompositeDisposable();


        // GETTING EXTRAS FROM INTENT
        Intent intent = getIntent();
        Uri data = intent.getData();
        if (data != null) {
            System.out.println("----------------------------------------------GOT DEEP LINK :" + data + "---------------------");
            String link1 = data.toString();
            System.out.println("----------------------------------------------LINK PATH :" + link1 + "---------------------");
            String[] s1 = link1.split("&link=");
            for (String s : s1) {
                System.out.println("------------------------------DEEP LINK :" + s);
            }
            String[] s2 = s1[1].split("ext");
            link = Common.LOAD_BOOK_DETAILS_IP + s2[0];
            String[] s3 = s2[1].split("lng");
            for (String s : s3) {
                System.out.println("------------------------------DEEP LINK S3:" + s);
            }
            String[] s4 = s3[1].split("dwn");
            for (String s : s4) {
                System.out.println("------------------------------DEEP LINK S4:" + s);
            }
            size_text = s4[1];
            lang = s4[0];
            extension = s3[0];
            stringSplit[1] = s2[0];

            System.out.println("------------------------------ LINK :" + link);

            try {
                FirebaseAnalytics.getInstance(getBaseContext()).logEvent("SHARED_BOOK_OPENED", null);
                FirebaseInAppMessaging.getInstance().triggerEvent("SHARED_BOOK_OPENED");
            } catch (Exception e) {
                e.printStackTrace();
            }


            Common.ShowCover = true;
            Common.AllowDownload = true;

        } else {


            try {
                Intent i = getIntent();
                link = i.getStringExtra("link");
                size_text = i.getStringExtra("size");
                lang = i.getStringExtra("language");
                extension = i.getStringExtra("extension");
                stringSplit = link.split("/main/");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.out.println(" MD5 ===================================== " + stringSplit[1]);
        // checking that book is already dowloaded or not
        isBookAlreadyDownloaded = checkIfBookAlreadyDownloaded(stringSplit[1]);

        //INITIALIZING LAYOUT COMPONENTS
        language = findViewById(R.id.language);
        description = findViewById(R.id.description);
        download = findViewById(R.id.download);
        share = findViewById(R.id.share);

        ad_view_container = findViewById(R.id.ad_View_Container);
        appBarLayout = findViewById(R.id.app_bar_layout);
        book_detail_card_view_available = findViewById(R.id.book_detail_card_view_available);
        book_detail_card_view_language = findViewById(R.id.book_detail_card_view_language);
        topic = findViewById(R.id.topic);
        title = findViewById(R.id.title);
        cover = findViewById(R.id.cover);
        year = findViewById(R.id.year);
        author = findViewById(R.id.author);
        publisher = findViewById(R.id.publisher);
        isbn = findViewById(R.id.isbn);


        language.setText(lang);
        size_text = size_text.trim().replace(" ", "");

        // checking if book is already downloaded
        if (isBookAlreadyDownloaded) {
            download.setText("Downloaded");
            download.setEnabled(false);
        } else {

            System.out.println("CHECKING BOOK DOWNLOAD NAME -> IN ON CREATE - > ELSE");

            download.setText(new StringBuilder(" DOWNLOAD (").append(size_text).append(") "));

        }


        LoadBook loadBook = new LoadBook(this, link);

        File directory = new File(Environment.getExternalStorageDirectory() + File.separator + "images");
        directory.mkdirs();
        Log.v("FILE", Environment.getExternalStorageDirectory() + File.separator + "images");
        this.setTitle("Book Detail");

        loadBook.execute();

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareBook();
                Animation animation;
                animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink_animation);
                share.startAnimation(animation);

            }
        });

        adView = new AdView(this);
        adView.setAdUnitId("ca-app-pub-7936495263143595/5961829693");
        ad_view_container.addView(adView);

        loadBanner();

        if (!Common.AllowDownload) {
            download.setVisibility(View.GONE);

        }
        if (!Common.ShowCover) {
            share.setVisibility(View.GONE);
            topic.setText("-NA-");
        }


        download.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Dexter.withActivity(LoadBookDetail.this).withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse response) {
                                // if the book is already downloaded then we have to show this snackbar and return from there
                                if (download.getText().toString().contains("Downloaded")) {
                                    Snackbar snackbar = Snackbar.make(layout, "Book Already Downloaded, Please delete it first if you want to download it again", Snackbar.LENGTH_INDEFINITE);

                                    snackbar.setActionTextColor(Color.RED);
                                    snackbar.setAction("Okay", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            snackbar.dismiss();
                                        }
                                    });
                                    snackbar.show();
                                    return;
                                }

// when the book is not downloaded , means we have to initialize the download process
                                if (enableDownloadButton) {
                                    try {

                                        startDownload();

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        Toast.makeText(LoadBookDetail.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                    try {
                                        FirebaseAnalytics.getInstance(getBaseContext()).logEvent("DOWNLOAD_BUTTON_CLICKED", null);
                                        FirebaseInAppMessaging.getInstance().triggerEvent("DOWNLOAD_BUTTON_CLICKED");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {

                                    try {
                                        FirebaseAnalytics.getInstance(getBaseContext()).logEvent("DOWNLOAD_BUTTON_CLICKED", null);
                                        FirebaseInAppMessaging.getInstance().triggerEvent("DOWNLOAD_BUTTON_CLICKED");
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    download_button_clicked = true;
                                    Snackbar snackbar = Snackbar.make(LoadBookDetail.layout, "Please wait....generating Download Link", Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse response) {

                                ActivityCompat.requestPermissions(LoadBookDetail.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();

                if (ContextCompat.checkSelfPermission(LoadBookDetail.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {

                    show_WritePermission_Denied_Dialog();
                }

            }
        });

        appBarLayoutListener();


    }

    private void show_WritePermission_Denied_Dialog() {
        //Toast.makeText(getApplicationContext(),"Permission Not Granted",Toast.LENGTH_LONG).show();
        Snackbar snackbar = Snackbar.make(LoadBookDetail.layout, "Permission Not Granted. Enable it in settings", Snackbar.LENGTH_LONG);
        snackbar.setAction("Open Settings", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivityForResult(intent, REQUEST_PERMISSION_SETTING);

            }

        });

        snackbar.show();
    }

    private void shareBook() {


        Uri imageUri = getImageUri();
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/png");
        SpannableStringBuilder str = new SpannableStringBuilder(title.getText());
        str.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), 0, 0, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(stringSplit[1] + "ext" + extension + "lng" + lang + "dwn" + size_text))
                .setDomainUriPrefix("https://theworldofbooks.page.link/book/")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .buildDynamicLink();
        Uri dynamicLinkUri = dynamicLink.getUri();
        System.out.println("------------------------------" + dynamicLinkUri + "-------------------------------");
        String msg = "I am reading *" + str + "* on *World of Books*. \nOpen it Now : \n" + dynamicLinkUri + "\n";
        intent.putExtra(Intent.EXTRA_TEXT, msg);

        try {
            FirebaseAnalytics.getInstance(getBaseContext()).logEvent("BOOK_SHARED", null);
            FirebaseInAppMessaging.getInstance().triggerEvent("BOOK_SHARED");
        } catch (Exception e) {
            e.printStackTrace();
        }
        startActivity(intent);

    }

    private Uri getImageUri() {
        cover.setDrawingCacheEnabled(true);
        cover.buildDrawingCache();
        Bitmap image = null;
        try {
            image = ((BitmapDrawable) cover.getDrawable()).getBitmap();
        } catch (Exception e) {
            e.printStackTrace();
        }

        File imagesFolder = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Uri uri = null;
        try {
            imagesFolder.mkdirs();
            File file = new File(imagesFolder, "shared_image.png");

            FileOutputStream stream = new FileOutputStream(file);
            assert image != null;
            image.compress(Bitmap.CompressFormat.PNG, 50, stream);
            stream.flush();
            stream.close();
            uri = FileProvider.getUriForFile(this, "com.collegetime.theworldofbooks.fileprovider", file);

        } catch (IOException e) {
            Log.d("SHR_IMG", "IOException while trying to write file for sharing: " + e.getMessage());
        }
        return uri;
    }


    private void appBarLayoutListener() {
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (verticalOffset == 0) {
                    book_detail_card_view_available.setVisibility(View.VISIBLE);
                    book_detail_card_view_language.setVisibility(View.VISIBLE);

                } else if (Math.abs(verticalOffset) >= 0.68 * appBarLayout.getTotalScrollRange()) {
                    book_detail_card_view_available.setVisibility(View.GONE);
                    book_detail_card_view_language.setVisibility(View.GONE);

                } else {
                    book_detail_card_view_language.setVisibility(View.VISIBLE);
                    book_detail_card_view_available.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void loadBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();

        ////         F O R   T E S T    A D S
//        List<String> testDeviceIds = Arrays.asList("8A39D0B98F696F49D91F1AC65FA51CD8","D8A9984CEF4DD29F505CC0234D94D473");
//        RequestConfiguration configuration =
//                new RequestConfiguration.Builder().setTestDeviceIds(testDeviceIds).build();
//        MobileAds.setRequestConfiguration(configuration);

        //   Use RequestConfiguration.Builder().setTestDeviceIds(Arrays.asList("8A39D0B98F696F49D91F1AC65FA51CD8")

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);

        adView.loadAd(adRequest);


        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);

                enableDownloadButton = true;
                //      Toast.makeText(LoadBookDetail.this,"AD failed to load",Toast.LENGTH_LONG).show();
                if (download_button_clicked) {
                    try {
                        startDownload();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(LoadBookDetail.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }


            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                enableDownloadButton = true;
                //  Toast.makeText(LoadBookDetail.this,"AD Loaded",Toast.LENGTH_LONG).show();
                Log.e("ADVIEW", " ............... AD LOADeD////////////////");
                if (download_button_clicked) {
                    try {
                        startDownload();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(LoadBookDetail.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });


    }

    private AdSize getAdSize() {

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        EventBus.getDefault().postSticky(new HideFabEvent(true));
        EventBus.getDefault().postSticky(new showTitleEvent(true));
        finish();
    }


    private void startDownload() throws IOException {
        if (!isMyServiceRunning()) {

            Snackbar snackbar = Snackbar.make(LoadBookDetail.layout, title.getText() + " download in progress", Snackbar.LENGTH_LONG);
            snackbar.show();
//        Intent i = new Intent(LoadBookDetail.this, DownloadService.class);
//        i.putExtra("link", downloadLink);
//        i.putExtra("extension", extension);
//        i.putExtra("title", title.getText());
//        startService(i);


            checkFirstDownload();
            // TO SAVE IMAGE WHICH IS BEING SHOWN IN THE IMAGEVIEW OF BOOK DETAIL
            cover.setDrawingCacheEnabled(true);
            cover.buildDrawingCache();
            Bitmap image = null;
            try {
                image = ((BitmapDrawable) cover.getDrawable()).getBitmap();
            } catch (Exception e) {
                e.printStackTrace();
            }

            File rootImageStorage = new File(String.valueOf(getApplicationContext().getExternalFilesDir("storage/emulated/0/WorldOfBooks/Image")));
            if (!rootImageStorage.exists()) {
                rootImageStorage.mkdirs();
            }
            File downloadImageFile = new File(rootImageStorage.getAbsolutePath() + "/" + title.getText().toString().trim() + ".jpg");


            try {
                downloadImageFile.createNewFile();
                Common.BOOK_IMAGE = downloadImageFile.getAbsolutePath();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                FileOutputStream out = new FileOutputStream(downloadImageFile);
                assert image != null;
                image.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {
            Snackbar snackbar = Snackbar.make(LoadBookDetail.layout, "Another download is in progress, Please try after that download is finished.", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

        testDownload(downloadLink, extension, title.getText().toString());
        //   testDownloadManager(downloadLink, extension, title.getText().toString());

    }


    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (DownloadService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    private void checkFirstDownload() {
        preferences = getSharedPreferences("AppData", MODE_PRIVATE);
        editor = preferences.edit();
        String firstDownload = preferences.getString("firstDownload", "");
        if (firstDownload.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoadBookDetail.this, R.style.MessageAlertDialogStyle);
            builder.setCancelable(false);
            builder.setMessage("DO NOT CLOSE the application , please keep the application in background while download is in progress, as it may affect the download.");
            builder.setTitle("!!! Download May be affected !!!");
            builder.setPositiveButton("GOT IT", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    editor.putString("firstDownload", "done");
                    editor.apply();
                    dialog.dismiss();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog1) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(android.R.color.holo_red_light));
                }
            });
            dialog.show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_PERMISSION_SETTING:
                if (ContextCompat.checkSelfPermission(LoadBookDetail.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    if (enableDownloadButton) {
                        try {
                            startDownload();
                        } catch (IOException e) {
                            e.printStackTrace();
                            Toast.makeText(LoadBookDetail.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else {


                        download_button_clicked = true;
                        Snackbar snackbar = Snackbar.make(LoadBookDetail.layout, "Please wait....generating Download Link", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                } else {
                    show_WritePermission_Denied_Dialog();
                }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            FirebaseAnalytics.getInstance(getBaseContext()).logEvent("LOAD_BOOK_DETAIL_ACTIVITY_OPENED", null);
            FirebaseInAppMessaging.getInstance().triggerEvent("LOAD_BOOK_DETAIL_ACTIVITY_OPENED");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void testDownloadManager(String downloadLink, String extension, String bookName) throws IOException {


        Uri downloadUri = Uri.parse(downloadLink);
        Uri pathUri = null;
        try {
            pathUri = Uri.parse(getBookPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        downloadRequest = new DownloadManager.Request(downloadUri)
                .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI)
                .setAllowedOverMetered(true)
                .setAllowedOverRoaming(true)
                .setTitle("File Download")
                .setVisibleInDownloadsUi(true)
                .setDescription("nothing")
                .setDestinationInExternalPublicDir(getBookPath(), stringSplit[1] + extension);

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        lastDownload = downloadManager.enqueue(downloadRequest);
        queryStatus();


    }

    public void queryStatus() {
        Cursor c = downloadManager.query(new DownloadManager.Query().setFilterById(lastDownload));

        if (c == null) {
            Toast.makeText(this, "Download not found!", Toast.LENGTH_LONG).show();
        } else {
            c.moveToFirst();

            Log.d(getClass().getName(), "COLUMN_ID: " +
                    c.getLong(c.getColumnIndex(DownloadManager.COLUMN_ID)));
            Log.d(getClass().getName(), "COLUMN_BYTES_DOWNLOADED_SO_FAR: " +
                    c.getLong(c.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR)));
            Log.d(getClass().getName(), "COLUMN_LAST_MODIFIED_TIMESTAMP: " +
                    c.getLong(c.getColumnIndex(DownloadManager.COLUMN_LAST_MODIFIED_TIMESTAMP)));
            Log.d(getClass().getName(), "COLUMN_LOCAL_URI: " +
                    c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)));
            Log.d(getClass().getName(), "COLUMN_STATUS: " +
                    c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS)));
            Log.d(getClass().getName(), "COLUMN_REASON: " +
                    c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON)));

            Toast.makeText(this, statusMessage(c), Toast.LENGTH_LONG).show();
        }
    }

    private String statusMessage(Cursor c) {
        String msg = "???";

        switch (c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS))) {
            case DownloadManager.STATUS_FAILED:
                msg = "Download failed!";
                break;

            case DownloadManager.STATUS_PAUSED:
                msg = "Download paused!";
                break;

            case DownloadManager.STATUS_PENDING:
                msg = "Download pending!";
                break;

            case DownloadManager.STATUS_RUNNING:
                msg = "Download in progress!";
                break;

            case DownloadManager.STATUS_SUCCESSFUL:
                msg = "Download complete!";
                break;

            default:
                msg = "Download is nowhere in sight";
                break;
        }
        downloadRequest.setDescription(msg);

        return (msg);
    }


    private void testDownload(String downloadLink, String extension, String bookName) throws IOException {
        // this is only for test purpose
        String CHANNEL_ID = "Download";
//
//
//// in test ends
//
//
        Uri downloadUri = Uri.parse(downloadLink);
        Uri destinationUri = Uri.parse(this.getExternalCacheDir().toString() + extension);

        DownloadRequest downloadRequest = new DownloadRequest(downloadUri)
                .setRetryPolicy(new RetryPolicy() {
                    @Override
                    public int getCurrentTimeout() {
                        return 15000;
                    }

                    @Override
                    public int getCurrentRetryCount() {
                        return 10;
                    }

                    @Override
                    public float getBackOffMultiplier() {
                        return 2;
                    }

                    @Override
                    public void retry() throws RetryError {


                    }
                })
                .setDestinationURI(Uri.parse(getBookPath())) // this is to set path
                .setPriority(DownloadRequest.Priority.HIGH)
                .setDownloadResumable(true)
                .setDownloadContext(getApplicationContext())//Optional
                .setStatusListener(new DownloadStatusListenerV1() {
                    @SuppressLint("RestrictedApi")
                    @Override
                    public void onDownloadComplete(DownloadRequest downloadRequest) {

                        Common.thinDownloadManager.release();
                        System.out.println("DOWNLOAD ID in Completed =====================" + downloadRequest.getDownloadId());

                        Intent i = new Intent(LoadBookDetail.this, HomeActivity.class);
                        i.putExtra("isInternetAvailable", "No");
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(LoadBookDetail.this);
                        stackBuilder.addNextIntentWithParentStack(i);
                        PendingIntent resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                        builder.setContentIntent(resultIntent);
                        builder.mActions.clear();
                        builder.setContentText("File Downloaded, tap to view").setProgress(0, 0, false);
                        mNotification.notify(downloadRequest.getDownloadId(), builder.build());
                        addDownloadedFileToDatabase(downloadRequest.getDownloadId());


                    }

                    @Override
                    public void onDownloadFailed(DownloadRequest downloadRequest, int errorCode, String errorMessage) {
                        Toast.makeText(LoadBookDetail.this, "Download Failed" + errorMessage, Toast.LENGTH_LONG).show();
                        System.out.println(" IN TEST DOWNLOAD==================== -> onDownloadFailed" + errorMessage);

                        builder.setContentText("File Download Failed, Please Try Again").setProgress(0, 0, false);
                        mNotification.notify(downloadRequest.getDownloadId(), builder.build());

                    }

                    @Override
                    public void onProgress(DownloadRequest downloadRequest, long totalBytes, long downloadedBytes, int progress) {
                        System.out.println(" IN TEST DOWNLOAD progress====================" + String.valueOf(progress));


                        if (progress >= 100) {
                            Intent i = new Intent(LoadBookDetail.this, HomeActivity.class);
                            i.putExtra("isInternetAvailable", "No");
                            TaskStackBuilder stackBuilder = TaskStackBuilder.create(LoadBookDetail.this);
                            stackBuilder.addNextIntentWithParentStack(i);
                            PendingIntent resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                            builder.setContentIntent(resultIntent);
                            builder.setContentText("File Downloaded, tap to view").setProgress(0, 0, false);
                            mNotification.notify(downloadRequest.getDownloadId(), builder.build());
                        }


                        builder.setContentText("Downloaded " + progress + "%");
                        builder.setProgress(100, progress, false);
//                        builder.addAction(R.drawable.ic_cancel_black_24dp,"Cancel", cancelIntent);
//                        builder.addAction(R.drawable.ic_baseline_pause_24,"Pause",pauseIntent);

                        mNotification.notify(downloadRequest.getDownloadId(), builder.build());
                    }
                });
        if (!Common.thinDownloadManager.isReleased()) {
            Common.thinDownloadManager.add(downloadRequest);
        } else {
            Common.thinDownloadManager = new ThinDownloadManager();
            Common.thinDownloadManager.add(downloadRequest);
        }

        int downloadId = downloadRequest.getDownloadId();
        System.out.println("DOWNLOAD ID when added request =====================" + downloadId);


        BookItem bookItem = new BookItem();
        bookItem.setBookPath(Common.BOOK_PATH);
        bookItem.setBookName(title.getText().toString());
        bookItem.setCurrentlyReadingBook(false);
        bookItem.setFavourateBook(false);
        bookItem.setBookId(stringSplit[1]);
        bookItem.setBookImage(Common.BOOK_IMAGE);

        bookItem.setBookExt(extension);
        bookItem.setCompleted_currently_ReadingBook(false);


        Common.downloadQueueMap.put(downloadId, bookItem);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel = new NotificationChannel(CHANNEL_ID, "Download", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription("Download");
            NotificationManager manager = getApplicationContext().getSystemService(NotificationManager.class);
            assert manager != null;
            manager.createNotificationChannel(channel);
        }
        mNotification = NotificationManagerCompat.from(getApplicationContext());

        // this intent is to cancel download on notification cancel button click

        Intent cancelIntent = new Intent(LoadBookDetail.this, NotificationReceiver.class);
        cancelIntent.putExtra("id", downloadId);
        cancelIntent.putExtra(Common.PAUSE_DOWNLOAD, false);
        cancelIntent.putExtra(Common.CANCEL_DOWNLOAD, true);
        PendingIntent cancelPendingIntent = PendingIntent.getBroadcast(getBaseContext(), downloadId, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        // this intent is to pause download on notification pause button click
        Intent pause = new Intent(LoadBookDetail.this, NotificationReceiver.class);
        pause.putExtra("id", downloadId);
        pause.putExtra(Common.PAUSE_DOWNLOAD, true);
        pause.putExtra(Common.CANCEL_DOWNLOAD, false);
        PendingIntent pausePendingIntent = PendingIntent.getBroadcast(getBaseContext(), downloadId, pause, PendingIntent.FLAG_UPDATE_CURRENT);

        builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setContentText("Starting Download, this may take a while").setContentTitle("File Download").setSmallIcon(R.drawable.ic_file_download_black_24dp);
        builder.setPriority(Notification.PRIORITY_LOW);

        // this intent is to open app on notification click because the book is not shown yet in downloads so
        // we will no open downloads but open app
        Intent openApp = new Intent(LoadBookDetail.this, HomeActivity.class);
        openApp.putExtra("isInternetAvailable", "Yes");
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(LoadBookDetail.this);
        stackBuilder.addNextIntentWithParentStack(openApp);
        PendingIntent resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        builder.setContentIntent(resultIntent);
        builder.addAction(R.drawable.ic_cancel_black_24dp, "Cancel", cancelPendingIntent);
        // builder.addAction(R.drawable.ic_baseline_pause_24,"Pause",pausePendingIntent);
        mNotification.notify(downloadId, builder.build());


        // A B O V E     C O D  E          I S          W O R K I N G    B U T    IS S U E S       I N     S O M E     F I L E S

//        String piUrl= downloadLink;
//
//String dirPath=getBookPath();
//String fileName= bookName+extension;
//
//
//        int downloadId = PRDownloader.download(piUrl, dirPath, fileName)
//                .build()
//                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
//                    @Override
//                    public void onStartOrResume() {
//                        System.out.println("PI ======================================== DOWNLOAD STARTED  ");
//
//                    }
//                })
//                .setOnPauseListener(new OnPauseListener() {
//                    @Override
//                    public void onPause() {
//
//                    }
//                })
//                .setOnCancelListener(new OnCancelListener() {
//                    @Override
//                    public void onCancel() {
//
//                    }
//                })
//                .setOnProgressListener(new OnProgressListener() {
//                    @Override
//                    public void onProgress(Progress progress) {
//                        System.out.println("PI ======================================== DOWNLOAD PROGRESS = "+progress.toString());
//                    }
//                })
//                .start(new OnDownloadListener() {
//                    @Override
//                    public void onDownloadComplete() {
//System.out.println("PI ======================================== DOWNLOAD COMPLETE");
//                    }
//
//                    @Override
//                    public void onError(com.downloader.Error error) {
//                        System.out.println("PI ======================================== DOWNLOAD ERROR = "+error.getServerErrorMessage());
//                    }
//
//
//                });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//// FETCH STARTS FROM HERE
//        i++;
//        Toast.makeText(LoadBookDetail.this,"Above fetch start i = "+String.valueOf(i),Toast.LENGTH_LONG).show();
//
//        String url = downloadLink;
//        String file = getBookPath();
//System.out.println(url);
//        final Request request = new Request(url, file);
//        request.setPriority(Priority.HIGH);
//        request.setNetworkType(NetworkType.ALL);
//
//
//
//
//        fetch.enqueue(request, updatedRequest -> {
//
//
//            System.out.println("FETCH -------------------------------- ENQUEUD THE REQUEST"+bookName);
//        }, error -> {
//            //An error occurred enqueuing the request.
//            System.out.println("FETCH --------------------------------  ERROR IN THE REQUEST ENQUE " + error.getHttpResponse().toString());
//
//        });
//
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            channel = new NotificationChannel(CHANNEL_ID, "Download", NotificationManager.IMPORTANCE_LOW);
//            channel.setDescription("Download");
//            NotificationManager manager = getApplicationContext().getSystemService(NotificationManager.class);
//            assert manager != null;
//            manager.createNotificationChannel(channel);
//        }
//        mNotification = NotificationManagerCompat.from(getApplicationContext());
//        builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID).setContentText("Starting Download, this may take a while").setContentTitle("File Download").setSmallIcon(R.drawable.ic_file_download_black_24dp);
//        builder.setPriority(Notification.PRIORITY_LOW);
//        // builder.setContentIntent(resultIntent);
//        // builder.addAction(R.drawable.ic_cancel_black_24dp,"Cancel",resultIntent);
//        mNotification.notify(request.getId(), builder.build());
//
//        fetch.addListener(this);
    }

    @SuppressLint("RestrictedApi")
    private void addDownloadedFileToDatabase(int download_id) {

        BookItem bookItem = new BookItem();

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);


        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String downloadDate = df.format(c); // this is the download date

        Calendar cal = Calendar.getInstance();

        if (Common.BOOK_EXPIRY_OFFER)
        {            // in some offer we are giving double validity ( 1 month )
            cal.add(Calendar.MONTH, 1);
        }
        else{
            // in normal we are giving 15 days validity
            cal.add(Calendar.DATE, 15);
        }

        Date validTill = cal.getTime();
        String expiryDate = df.format(validTill); // on this date this download will expire

        for (Map.Entry<Integer, BookItem> downloadQueue : Common.downloadQueueMap.entrySet()) {
            if (download_id == downloadQueue.getKey()) {
                bookItem = downloadQueue.getValue();
                System.out.println("IN MAP============================ book name =" + bookItem.getBookName());
            }


        }

        bookItem.setDownload_date(downloadDate);
        bookItem.setDownload_expiry_date(expiryDate);
        try {
            System.out.println("============================ book download date =" + Common.convertToDate(bookItem.getDownload_date()));
            System.out.println("============================ book download EXPIRY date =" + Common.convertToDate(bookItem.getDownload_expiry_date()));
        } catch (ParseException e) {
            e.printStackTrace();
        }


//        BookItem bookItem = new BookItem();
//        bookItem.setBookPath(Common.BOOK_PATH);
//        bookItem.setBookName(title.getText().toString());
//        bookItem.setCurrentlyReadingBook(false);
//        bookItem.setFavourateBook(false);
//        bookItem.setBookId(stringSplit[1]);
//        bookItem.setBookImage(Common.BOOK_IMAGE);
//        bookItem.setBookExt(extension);


//        bookItem.setCompleted_currently_ReadingBook(false);
        FirebaseAnalytics.getInstance(getBaseContext()).logEvent("BOOK_SUCCESSFULLY_DOWNLOADED", null);
        FirebaseInAppMessaging.getInstance().triggerEvent("BOOK_SUCCESSFULLY_DOWNLOADED");

        Log.d("Download", "...........BOOK_IMAGE.........." + Common.BOOK_IMAGE);

        compositeDisposable.add(bookDataSource.InsertBook(bookItem).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(() -> {


            try {
                FirebaseAnalytics.getInstance(getBaseContext()).logEvent("BOOK_SUCCESSFULLY_DOWNLOADED", null);
                FirebaseInAppMessaging.getInstance().triggerEvent("BOOK_SUCCESSFULLY_DOWNLOADED");
            } catch (Exception e) {
                e.printStackTrace();
            }

        }, throwable -> {
            Log.e("INSERTION ERROR ", "Failed to insert in downloaded books database -> DownloadService ->postExecute");
            Toast.makeText(LoadBookDetail.this, "Some Error Occurred, Please Try again", Toast.LENGTH_SHORT).show();
        }));

        EventBus.getDefault().postSticky(new DownloadCountEvent(true));

        Intent i = new Intent(LoadBookDetail.this, HomeActivity.class);
        i.putExtra("isInternetAvailable", "No");
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(LoadBookDetail.this);
        stackBuilder.addNextIntentWithParentStack(i);
        PendingIntent resultIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
        }

        builder.setContentIntent(resultIntent);
        builder.setPriority(NotificationCompat.PRIORITY_MAX);
        builder.setContentText("File Downloaded, tap to view").setProgress(0, 0, false).setPriority(Notification.PRIORITY_MAX);
        builder.mActions.clear();
        mNotification.notify(download_id, builder.build());
        Snackbar snackbar = Snackbar.make(LoadBookDetail.layout, "Book Downloaded", Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    public String getBookPath() throws IOException {
        String bookPath;

        File rootStorage = new File(String.valueOf(getApplicationContext().getExternalFilesDir("storage/emulated/0/WorldOfBooks")));
        if (!rootStorage.exists()) {
            rootStorage.mkdirs();
        }


        File downloadedFile = new File(rootStorage.getAbsolutePath() + "/" + stringSplit[1] + "." + extension);


        if (!downloadedFile.exists()) {
            downloadedFile.createNewFile();
        }


        Common.BOOK_NAME = title.getText().toString();
        Common.BOOK_PATH = rootStorage.getAbsolutePath() + "/" + stringSplit[1] + "." + extension;

        // for testing

        //  bookPath =rootStorage.getAbsolutePath();
        bookPath = Common.BOOK_PATH;
        System.out.println(bookPath + "-====================-========== book path");
        return bookPath;
    }


    // below files are not of use
    @Override
    public void onAdded(@NotNull Download download) {
        Toast.makeText(LoadBookDetail.this, "Download Added", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCancelled(@NotNull Download download) {
        Toast.makeText(LoadBookDetail.this, "Download cancelled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleted(@NotNull Download download) {
        fetch.removeListener(this);
        fetch.close();

        System.out.println("Download Completed==========================  D O W N L O A  D      C O M P L  E T E D ");

        Toast.makeText(LoadBookDetail.this, "Download Completed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleted(@NotNull Download download) {

    }

    @Override
    public void onDownloadBlockUpdated(@NotNull Download download, @NotNull DownloadBlock downloadBlock, int i) {
        Toast.makeText(LoadBookDetail.this, "Download BLock Updated", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {
        System.out.println("Download in onError========================== " + download.getError() + " Progress = " + download.getProgress() + " msg:" + throwable.getMessage());
    }

    @Override
    public void onPaused(@NotNull Download download) {

    }

    @Override
    public void onProgress(@NotNull Download download, long l, long l1) {
        System.out.println("Download P R O G R E S S ========================== " + download.getProgress());

        Toast.makeText(LoadBookDetail.this, "Download progress = " + String.valueOf(download.getProgress()), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onQueued(@NotNull Download download, boolean b) {
        Toast.makeText(LoadBookDetail.this, "Download Queued", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRemoved(@NotNull Download download) {
        Toast.makeText(LoadBookDetail.this, "Download Removed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResumed(@NotNull Download download) {

    }

    @Override
    public void onStarted(@NotNull Download download, @NotNull List<? extends DownloadBlock> list, int i) {
        Toast.makeText(LoadBookDetail.this, "Download Started", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onWaitingNetwork(@NotNull Download download) {
        Toast.makeText(LoadBookDetail.this, "Download Waiting for Network", Toast.LENGTH_SHORT).show();
    }


    private boolean checkIfBookAlreadyDownloaded(String md5) {


        compositeDisposable.add(bookDataSource.searchBook(md5)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<BookItem>>() {
                    @Override
                    public void accept(List<BookItem> bookItems) throws Exception {

                        if (null != bookItems && bookItems.size() != 0) {
                            isBookAlreadyDownloaded = true;
                            download.setText(" Downloaded ");

                            System.out.println(" CHECKING BOOK DOWNLOAD NAME -> in if =" + bookItems.get(0).getBookName());

                        } else {
                            System.out.println(" CHECKING BOOK DOWNLOAD NAME -> in else =" + bookItems.get(0).getBookName());
                            download.setText("Download" + size_text);
                            isBookAlreadyDownloaded = false;
                        }

                    }
                }, throwable -> {
                    System.out.println(" CHECKING BOOK DOWNLOAD NAME -> in throwable");
                    download.setEnabled(true);
                    //  download.setText("Download"+size_text);
                    isBookAlreadyDownloaded = false;
                }));
//
        System.out.println(" CHECKING BOOK DOWNLOAD NAME -> RETURN VALUE = " + isBookAlreadyDownloaded);

        return isBookAlreadyDownloaded;
    }
}
